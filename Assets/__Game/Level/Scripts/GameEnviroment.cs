﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public sealed class GameEnviroment
{
    private static GameEnviroment _instance;
    private List<GameObject> _patrolPoints = new List<GameObject>();
    public List<GameObject> PatrolPoints
    {
        get { return _patrolPoints; }
    }
    public static GameEnviroment singleton
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameEnviroment();
                _instance.PatrolPoints.AddRange(GameObject.FindGameObjectsWithTag("PatrolPoint"));

                _instance._patrolPoints = _instance._patrolPoints.OrderBy(point => point.name).ToList();
            }
            
            return _instance;
        }
    }
}
