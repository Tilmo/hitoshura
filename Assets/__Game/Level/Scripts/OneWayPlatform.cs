﻿using __Game.Player.Scripts;
using UnityEngine;

namespace __Game.Level.Scripts
{
    public class OneWayPlatform : MonoBehaviour
    {
        public bool isUp;
        private PlayerController _playerController;
        private Collider _collider;

        void Start()
        {
            _collider = transform.parent.GetComponent<Collider>();
            _playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        }
    
        void Update()
        {
            if (_playerController.state.grounded && _playerController.fastFallValue > 0.8f)
            {
                _collider.enabled = false;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _collider.enabled = isUp;
            }
        }
    }
}
