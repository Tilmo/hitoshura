﻿using UnityEngine;

namespace __Game.Player.Scripts
{
    public class PlayerAnimationController : MonoBehaviour
    {
        private Animator _animator;
        private void Awake() => _animator = GetComponent<Animator>();

        public void Walk(bool move) => _animator.SetBool("Walking", move); 
        public void Jump(bool jump) => _animator.SetBool("Jumping", jump);
        public void Roll() => _animator.SetTrigger("Roll");
        public void AirRoll() => _animator.SetTrigger("AirRoll");
        public void IsRolling(bool rolling) => _animator.SetBool("IsRolling", rolling);
        public void IsWallSliding(bool sliding) => _animator.SetBool("IsWallSliding", sliding);

        public void Attack1() => _animator.SetTrigger("Attack1");
        public void Attack2() => _animator.SetTrigger("Attack2");
        public void Attack3() => _animator.SetTrigger("Attack3");

        public void DoubleJump(bool jump) => _animator.SetBool("DoubleJump", jump);

        public void Dead(bool dead) => _animator.SetBool("Dead", dead);

        public void CanMove(bool canMove) => _animator.SetBool("CanMove", canMove);
        public void CanAttack(bool canAttack) => _animator.SetBool("CanAttack", canAttack);

        public void IsAttacking(bool isAttacking) => _animator.SetBool("IsAttacking", isAttacking);

        public void Jump() => _animator.SetTrigger("Jump");

        public void Fastfall(bool fastfall) => _animator.SetBool("Fastfall", fastfall);

        public void MoveValue(float moveValue) => _animator.SetFloat("MoveValue", moveValue);

        public void AttackYValue(float attackYValue) => _animator.SetFloat("AttackYValue", attackYValue);

        public void IsSlowFalling(bool slowFall) => _animator.SetBool("IsSlowFalling", slowFall);
        
        public void AttackXValue(float attackValue) => _animator.SetFloat("AttackXValue", attackValue);
        
        public void AttackBValue(float attackValue) => _animator.SetFloat("AttackBValue", attackValue);
    }
}
