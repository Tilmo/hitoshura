﻿using System.Collections;
using System.Collections.Generic;
using __Game.Player.Scripts;
using UnityEngine;
using UnityEngine.AI;

public class Recoil : MonoBehaviour
{

    [SerializeField] private float stepsRecoiled;
    [SerializeField] private float recoilSteps;
    [SerializeField] private float recoilSpeed;

    [SerializeField] private float defaultRecoilSteps;
    [SerializeField] private float defaultRecoilSpeed;


    private Vector3 _directionModifier;
    
    
    
    //UNIVERSAL
    public void SetDirectionModifier(Vector3 direction)    
    {
        _directionModifier = direction;
    }
    
    private void DefaultRecoil()
    {
        recoilSpeed = defaultRecoilSpeed;
        recoilSteps = defaultRecoilSteps;
    }

    public void SetRecoilValues(float steps, float speed)
    {
        recoilSpeed = speed;
        recoilSteps = steps;
    }
    
    //FOR PLAYER STATES
    public void ProcessRecoilTimer(PlayerState playerState)
    {
        if (playerState.recoilX && stepsRecoiled < recoilSteps)
            stepsRecoiled+=Time.deltaTime;

        else
        {
            StopRecoil(playerState);
        }
    }
    public void ProcessRecoil(PlayerState playerState, Rigidbody rb)
    {
        if (playerState.recoilX)
        {
            if(playerState.facingRight)
                rb.velocity = new Vector3(-recoilSpeed, 0);

            else
            {
                rb.velocity = new Vector3(recoilSpeed, 0);
            }
        }
    }
    public void StopRecoil(PlayerState playerState)
    {
        stepsRecoiled = 0;
        //DefaultRecoil();
        playerState.recoilX = false;
    }
    
    
    //FOR ENEMY STATES
    
    public void ProcessRecoilTimer(EnemyState enemyState)
    {
        if (enemyState.recoilX && stepsRecoiled < recoilSteps)
            stepsRecoiled+=Time.deltaTime;

        else
        {
            StopRecoil(enemyState);
        }
    }

    public void ProcessRecoil(EnemyState enemyState, Rigidbody rb)
    {
        if (enemyState.recoilX)
        {
            rb.velocity = new Vector3(-recoilSpeed * -_directionModifier.normalized.x, 0);
        }
    }

    public void ProcessRecoil(EnemyState enemyState, NavMeshAgent agent)
    {
        if (enemyState.recoilX)
        {
            agent.updateRotation = false;
            agent.velocity = new Vector3(-recoilSpeed * -_directionModifier.normalized.x, 0);
        }
    }

    public void StopRecoil(EnemyState enemyState)
    {
        enemyState.recoilX = false;
        DefaultRecoil();
        stepsRecoiled = 0;
    }
    
    
}
