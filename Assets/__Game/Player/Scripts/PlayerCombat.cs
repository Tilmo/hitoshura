﻿using __Game.Enemy.Scripts;
using UnityEngine;

namespace __Game.Player.Scripts
{
    public class PlayerCombat : MonoBehaviour
    {
        private PlayerChildren _children;
        public PlayerState playerState;
        private PlayerController _player;


        private void Start()
        {
            _player = GetComponentInParent<PlayerController>();
            _children = _player.playerChildren;

        }
        
        public void SetCanMoveFalse()
        {
            playerState.rooted = true;
        }
        
        public void SetCanMoveTrue()
        {
            playerState.rooted = false;
        }
        
        public void SetCanCancelTrue()
        {
            _player.SetCanCancel(true);
        }

        public void SetCanCancel()
        {
            _player.SetCanCancel(false);
        }

        public void X1_SetAttackDisable()
        {
            _children.attackPointX1.SetActive(false);
        }
        public void X2_SetAttackDisable()
        {
            _children.attackPointX2.SetActive(false);
        }
        public void X3_SetAttackDisable()
        {
            _children.attackPointX3.SetActive(false);
        }
        public void Y1_SetAttackDisable()
        {
            _children.attackPointY1.SetActive(false);
        }
        public void Y2_SetAttackDisable()
        {
            _children.attackPointY2.SetActive(false);
        }
        public void Y3_SetAttackDisable()
        {
            _children.attackPointY3.SetActive(false);
        }
        public void B1_SetAttackDisable()
        {
            _children.attackPointB1.SetActive(false);
        }
        public void B2_SetAttackDisable()
        {
            _children.attackPointB2.SetActive(false);
        }
        public void B3_SetAttackDisable()
        {
            _children.attackPointB3.SetActive(false);
        }

        public void SetAttacksDisabled()
        {
            SetCanCancel();
            
            X1_SetAttackDisable();
            X2_SetAttackDisable();
            X3_SetAttackDisable();
            
            Y1_SetAttackDisable();
            Y2_SetAttackDisable();
            Y3_SetAttackDisable();
    
            B1_SetAttackDisable();
            B2_SetAttackDisable();
            B3_SetAttackDisable();
        }


        public LayerMask enemyLayers;
    
        public void X1_SetAttackActive()
        {
            // _children.attackPointX1.SetActive(true);
            var hitEnemies =
                Physics.OverlapSphere(_children.attackPointX1.transform.position, 1f, enemyLayers);
        
        
            foreach (var enemy in hitEnemies)
            {
                if (enemy.name == "EnemyHealth")
                {
                    var healthComponent = enemy.GetComponent<EnemyDamageController>();
                    healthComponent.TakeDamage(0);
                    healthComponent.state.recoilX = true;
                }
                
                if (enemy.CompareTag("TrainingDummy"))
                {
                    var recoil = enemy.GetComponent<TrainingDummy>().recoil;
                    
                    if(recoil == null)
                        return;
                    
                    recoil.SetDirectionModifier(enemy.transform.position - transform.position);
                    recoil.SetRecoilValues(20, 5);
                    enemy.GetComponent<TrainingDummy>().Recoil();
                }
            }
        }
        
        public void X2_SetAttackActive()
        {
            // _children.attackPointX2.SetActive(true);
            var hitEnemies =
                Physics.OverlapSphere(_children.attackPointX2.transform.position, 1f, enemyLayers);
        
        
            foreach (var enemy in hitEnemies)
            {
                if (enemy.name == "EnemyHealth")
                {
                    var healthComponent = enemy.GetComponent<EnemyDamageController>();
                    healthComponent.TakeDamage(0);
                    healthComponent.state.recoilX = true;
                }
                
                if (enemy.CompareTag("TrainingDummy"))
                {
                    var recoil = enemy.GetComponent<TrainingDummy>().recoil;
                    
                    if(recoil == null)
                        return;
                    
                    recoil.SetDirectionModifier(enemy.transform.position - transform.position);
                    recoil.SetRecoilValues(30, 7);
                    enemy.GetComponent<TrainingDummy>().Recoil();
                }
            }
        }
        
        public void X3_SetAttackActive()
        {
            // _children.attackPointX3.SetActive(true);
            var hitEnemies =
                Physics.OverlapSphere(_children.attackPointX3.transform.position, 1f, enemyLayers);
        
        
            foreach (var enemy in hitEnemies)
            {
                if (enemy.name == "EnemyHealth")
                {
                    var healthComponent = enemy.GetComponent<EnemyDamageController>();
                    healthComponent.TakeDamage(0);
                    healthComponent.state.recoilX = true;
                }

                if (enemy.CompareTag("TrainingDummy"))
                {
                    var recoil = enemy.GetComponent<TrainingDummy>().recoil;
                    
                    if(recoil == null)
                        return;
                    
                    recoil.SetDirectionModifier(enemy.transform.position - transform.position);
                    recoil.SetRecoilValues(40, 10);
                    enemy.GetComponent<TrainingDummy>().Recoil();
                }

            }
        }

        public void Y1_SetAttackActive()
        {
            _children.attackPointY1.SetActive(true);
        
            var hitEnemies =
                Physics.OverlapSphere(_children.attackPointY1.transform.position, 1f, enemyLayers);
        
            foreach (var enemy in hitEnemies)
            {
                if (enemy.name == "EnemyHealth")
                { 
                    var healthComponent = enemy.GetComponent<EnemyDamageController>();
                    healthComponent.TakeDamage(1);
                    healthComponent.state.recoilX = true;
                }
                
                if (enemy.CompareTag("TrainingDummy"))
                {
                    var recoil = enemy.GetComponent<TrainingDummy>().recoil;
                    
                    if(recoil == null)
                        return;
                    
                    recoil.SetDirectionModifier(enemy.transform.position - transform.position);
                    recoil.SetRecoilValues(40, -12);
                    enemy.GetComponent<TrainingDummy>().Recoil();
                }
            }
        }
        public void Y2_SetAttackActive()
        {
            _children.attackPointY2.SetActive(true);
        
            var hitEnemies =
                Physics.OverlapSphere(_children.attackPointY2.transform.position, 1f, enemyLayers);
        
            foreach (var enemy in hitEnemies)
            {
                if (enemy.name == "EnemyHealth")
                { 
                    var healthComponent = enemy.GetComponent<EnemyDamageController>();
                    healthComponent.TakeDamage(1);
                    healthComponent.state.recoilX = true;
                }
                
                if (enemy.CompareTag("TrainingDummy"))
                {
                    var recoil = enemy.GetComponent<TrainingDummy>().recoil;
                    
                    if(recoil == null)
                        return;
                    
                    recoil.SetDirectionModifier(enemy.transform.position - transform.position);
                    recoil.SetRecoilValues(40, -12);
                    enemy.GetComponent<TrainingDummy>().Recoil();
                }
            }
        }
        public void Y3_SetAttackActive()
        {
            _children.attackPointY3.SetActive(true);
        
            var hitEnemies =
                Physics.OverlapSphere(_children.attackPointY3.transform.position, 1f, enemyLayers);
        
            foreach (var enemy in hitEnemies)
            {
                if (enemy.name == "EnemyHealth")
                { 
                    var healthComponent = enemy.GetComponent<EnemyDamageController>();
                    healthComponent.TakeDamage(1);
                    healthComponent.state.recoilX = true;
                }
                
                if (enemy.CompareTag("TrainingDummy"))
                {
                    var recoil = enemy.GetComponent<TrainingDummy>().recoil;
                    
                    if(recoil == null)
                        return;
                    
                    recoil.SetDirectionModifier(enemy.transform.position - transform.position);
                    recoil.SetRecoilValues(40, -12);
                    enemy.GetComponent<TrainingDummy>().Recoil();
                }
            }
        }

        public void B1_SetAttackActive()
        {
            _children.attackPointB1.SetActive(true);
        
            var hitEnemies =
                Physics.OverlapSphere(_children.attackPointB1.transform.position, 1f, enemyLayers);
        
            foreach (var enemy in hitEnemies)
            {
                if (enemy.name == "EnemyHealth")
                {
                    var healthComponent = enemy.GetComponent<EnemyDamageController>();
                    healthComponent.TakeDamage(2);
                    healthComponent.state.recoilX = true;
                }
                
                else if (enemy.CompareTag("TrainingDummy"))
                {
                    var recoil = enemy.GetComponent<TrainingDummy>().recoil;
                    
                    if(recoil == null)
                        return;
                    
                    recoil.SetDirectionModifier(enemy.transform.position - transform.position);
                    recoil.SetRecoilValues(60, 10);
                    enemy.GetComponent<TrainingDummy>().Recoil();
                }
            }
        }
        
        public void B2_SetAttackActive()
        {
            _children.attackPointB2.SetActive(true);
        
            var hitEnemies =
                Physics.OverlapSphere(_children.attackPointB2.transform.position, 1f, enemyLayers);
        
            foreach (var enemy in hitEnemies)
            {
                if (enemy.name == "EnemyHealth")
                {
                    var healthComponent = enemy.GetComponent<EnemyDamageController>();
                    healthComponent.TakeDamage(2);
                    healthComponent.state.recoilX = true;
                }
                
                else if (enemy.CompareTag("TrainingDummy"))
                {
                    var recoil = enemy.GetComponent<TrainingDummy>().recoil;
                    
                    if(recoil == null)
                        return;
                    
                    recoil.SetDirectionModifier(enemy.transform.position - transform.position);
                    recoil.SetRecoilValues(60, 10);
                    enemy.GetComponent<TrainingDummy>().Recoil();
                }
            }
        }
        
        public void B3_SetAttackActive()
        {
            _children.attackPointB3.SetActive(true);
        
            var hitEnemies =
                Physics.OverlapSphere(_children.attackPointB3.transform.position, 1f, enemyLayers);
        
            foreach (var enemy in hitEnemies)
            {
                if (enemy.name == "EnemyHealth")
                {
                    var healthComponent = enemy.GetComponent<EnemyDamageController>();
                    healthComponent.TakeDamage(2);
                    healthComponent.state.recoilX = true;
                }
                
            }
        }


    }
}
