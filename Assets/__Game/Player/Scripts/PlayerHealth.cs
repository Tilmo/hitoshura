﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private int maxHealth;
    [SerializeField] private int currentHealth;
    private SliderBar _sliderBar;
    [SerializeField] private GameObject healthBar;

    public delegate void Combo();

    public static event Combo BreakCombo;
    private void Start()
    {
        _sliderBar = healthBar.GetComponent<SliderBar>();
        currentHealth = maxHealth;
        _sliderBar.SetMaxValue(maxHealth);
        _sliderBar.SetValue(currentHealth);
    }

    public bool CheckIfDead()
    {
        return currentHealth < 1;
        
    }
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        _sliderBar.SetValue(currentHealth);
        BreakCombo?.Invoke();
    }

    public void HealthBar(bool active)
    {
        healthBar.SetActive(active);
    }
    
}
