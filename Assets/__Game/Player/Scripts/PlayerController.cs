﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace __Game.Player.Scripts
{
    public class PlayerController : MonoBehaviour
    {

        public PlayerState state;
        private Recoil _recoil;
        private PlayerControls _playerControls;
        private Rigidbody _rigidbody;
        private PlayerAnimationController _playerAnimationController;
        private Animator _animator;
        public PlayerChildren playerChildren;
        private PlayerHealth _health;
        private ComboController _combo;
        private GameController _gameController;

        [SerializeField] private PlayerCameras cameras;

        [SerializeField] private float moveSpeed = 5f;
        [SerializeField] private float jumpVelocity = 8f;
        [SerializeField] private float slowFallVelocity = 8f;
        [SerializeField] private int numberOfExtraJumps = 1;
        
        private int _jumpCounter;
        private bool _jumpBeingHeld;
        
        [SerializeField] private float fallMultiplier = 2.5f;
        [SerializeField] private float lowJumpMultiplier = 2f;
        [SerializeField] private float fastFallMultiplier = 2f;

        [SerializeField] private float rollVelocity = 15f;
        [SerializeField] private float airRollVelocity = 15f;
        [SerializeField] private float rollCooldown = 1f;
        [SerializeField] private Vector3 airRollDirection = new Vector3(1, 1, 0);
        
        
        private int _playerLayer;
        private int _playerIgnoreEnemyLayer;
        
        private float _stunTimer;
        
        public float wallSlidingSpeed;
        
        private float _moveValue;
        private bool _jumping;
        
        [SerializeField] private float yRotation;
        [SerializeField] private float eulerAnglesY;
        
        [SerializeField] private float rollXValue;




        
        private float _fixedDeltaTime;
        
        private void Awake()
        {
            InitializePlayerControls();
            this._fixedDeltaTime = Time.fixedDeltaTime;

            if (state == null)
                state = GetComponent<PlayerState>();

            _playerLayer = LayerMask.NameToLayer("Player");
            _playerIgnoreEnemyLayer = LayerMask.NameToLayer("PlayerIgnoreEnemy");
            
        }

        private void OnEnable()
        {
            _playerControls.Enable();
        }

        private void Start()
        {
            InitializeGameObjects();
            
            //transform.position = _gameController.lastCheckpointPosition;
        }

        private void FixedUpdate()
        {
            if (!state.stunned)
            {
                ProcessCombatRotation();
                ChangePlayerGravity_BasedOnPlayerControls();
            }
            
            _recoil.ProcessRecoilTimer(state);
        }

        private void LateUpdate()
        {
            ResetJumpCounter();
        }

        private void OnDisable()
        {
            _playerControls.Disable();
        }

        private bool CanFastFall()
        {
            return fastFallValue > 0.8f && !state.grounded && _rigidbody.velocity.y < 0;
        }



        private void Update()
        {
            ProcessEncounter();

            Time.fixedDeltaTime = this._fixedDeltaTime * Time.timeScale;
            
            
            ProcessPlayerFacing();
            _animator.SetBool("Grounded", state.grounded);
            _moveValue = _playerControls.gameplay.move.ReadValue<float>();
            fastFallValue = _playerControls.gameplay.fastfall.ReadValue<float>();

            if (CanFastFall())
                FastFall();

            _playerAnimationController.Walk(_moveValue != 0 && !state.rooted);
            _playerAnimationController.CanMove( !state.rooted );
            _playerAnimationController.Fastfall(state.fastFalling);
            RotatePlayerGraphics();
            
            if (state.wallSlide)
            {
                var velocity = _rigidbody.velocity;
                _rigidbody.velocity =
                    new Vector3(velocity.x, Mathf.Clamp(velocity.y, -wallSlidingSpeed, float.MaxValue));
            }
            
            ProcessWallSlide();
            
            _playerAnimationController.Jump(!state.grounded);
            _playerAnimationController.IsRolling(state.rolling);
            _playerAnimationController.IsWallSliding(state.wallSlide);
            
            state.attacking = _animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack");
            
            _playerAnimationController.DoubleJump(_animator.GetCurrentAnimatorStateInfo(0).IsTag("DoubleJump"));

            if (!state.attacking)
            {
                state.canAttack = true;

                if (!state.fastFalling)
                {
                    state.rooted = false;
                }
                state.canCancelAttack = true;

            }
            else
            {
                state.canAttack = false;
                state.rooted = true;
            }

            if (!state.grounded && !state.fastFalling && !state.rolling)
            {
                state.rooted = false;
            }

            if (!state.rolling)
            {
                RotateAndMoveCharacter();
            }
            
            _recoil.ProcessRecoil(state, _rigidbody);
            
        }

        ////----////\\\\----\\\\


        private void InitializePlayerControls()
        {
            state.canRoll = true;
            
            _playerControls = new PlayerControls();
            _playerControls.gameplay.jump.started += context =>
            {
                JumpCheck();
                _jumpBeingHeld = true;
            };
            _playerControls.gameplay.jump.canceled += context => { _jumpBeingHeld = false; };
            _playerControls.gameplay.attack1.performed += context => { if (state.canAttack || state.canCancelAttack) { Attack1();} };
            _playerControls.gameplay.attack2.performed += context => { if (state.canAttack || state.canCancelAttack) { Attack2();} };
            _playerControls.gameplay.attack3.performed += context => { if (state.canAttack || state.canCancelAttack) { Attack3();} };

            _playerControls.gameplay.roll.performed += context =>
            {
                if (!state.canRoll) return;

                if (state.grounded)
                {
                    Roll();
                }
                else if (!state.grounded)
                {
                    AirRoll();
                }
            };
        }
        private void InitializeGameObjects()
        {
            state = GetComponent<PlayerState>();
            _rigidbody = GetComponent<Rigidbody>();
            _recoil = GetComponent<Recoil>();
            _combo = GetComponent<ComboController>();
//            _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
            
            _playerAnimationController = GetComponentInChildren<PlayerAnimationController>();
            _animator = GetComponentInChildren<Animator>();
            _health = GetComponent<PlayerHealth>();
        }


        private bool _scoreResetTrigger;
        private void ProcessEncounter()
        {
            cameras.encounterCamera.SetActive(state.encounter);
            
            //_health.HealthBar(state.encounter);
            _combo.DisableUI(state.encounter);

            if (state.encounter)
            {
                _scoreResetTrigger = true;
            }

            if (!_scoreResetTrigger) return;
            if (state.encounter) return;
            _scoreResetTrigger = false;
            _combo.TakeDamage();
        }

        
        private void ProcessWallSlide()
        {
            state.wallSlide = state.walled && !state.grounded && GetMove().x != 0;
        }
        
        private void ProcessCombatRotation()
        {
            var rotation = transform.rotation;
            yRotation = rotation[1];
            eulerAnglesY = rotation.eulerAngles.y;
        }
        public float fastFallValue;

        
        private void ProcessPlayerFacing()
        {
            var controls = _playerControls.gameplay.move.ReadValue<float>();
            
            if (!state.recoilX && !state.rolling)
            {
                if (controls > 0)
                    state.facingRight = true;
            
                else if (controls < 0)
                    state.facingRight = false;
            }
  
        }
        

        private void RotatePlayerGraphics()
        {
            if (!state.rooted && !state.rolling && !state.attacking)
            {
                if ( yRotation == 0) 
                {
                    playerChildren.graphics.transform.rotation = Quaternion.Euler(0, -180, 0);
                    _playerAnimationController.MoveValue(1);
                }
                else if(yRotation == -1)
                {
                    playerChildren.graphics.transform.rotation = Quaternion.Euler(0, 180, 0);
                    _playerAnimationController.MoveValue(0);
                }
            }
        }
        private void FastFall()
        {
            state.fastFalling = true;
            state.rooted = true;
            StartCoroutine(FastFallWithWait());

            IEnumerator FastFallWithWait()
            {

                while (!state.grounded)
                {
                    var velocity = _rigidbody.velocity;
                    velocity -= Vector3.down * (Physics.gravity.y * (fastFallMultiplier - 1) * Time.fixedDeltaTime);
                    velocity = new Vector3(0f, velocity.y, 0);
                    _rigidbody.velocity = velocity;
                    yield return new WaitForSeconds(0.2f);
                }

                state.fastFalling = false;
                state.rooted = false;
            }
        }


        public void TakeDamage(int damage)
        {
            _health.TakeDamage(damage);

            if (_health.CheckIfDead())
            {
                _playerAnimationController.Dead(true);
                Time.timeScale = 0.1f;
                Debug.Log("Dead");
                Invoke(nameof(Die), 1f);
            }
        }

        private void Die()
        {
            Time.timeScale = 1f;
            
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        private float CalculateAttackYValue()
        {
            var tempValue = _attackYValue;
            _attackYValue += 0.5f;
            if (!(tempValue > 1)) return tempValue;
            _attackYValue = 0.5f;
            return 0;
        }

        private float CalculateAttackXValue()
        {
            var tempValue = _attackXValue;
            _attackXValue += 0.5f;
            if (!(tempValue > 1)) return tempValue;
            _attackXValue = 0.5f;
            return 0;
        }

        private float CalculateAttackBValue()
        {
            var tempValue = _attackBValue;
            _attackBValue += 0.5f;
            if (!(tempValue > 1)) return tempValue;
            _attackBValue = 0.5f;
            return 0;
        }

        private enum Attack
        {
            X1,
            X2,
            X3,
            Y1,
            Y2,
            Y3,
            B1,
            B2,
            B3
        };

        [SerializeField] private RecoilValues _values;
        
        private ValueTuple<float, float> AttackRecoil(Attack attack)
        {
            float steps = 0;
            float speed = 0;
            
            switch (attack)
            {
                case Attack.X1:
                    speed = _values.GetX1Speed();
                    steps = _values.GetX1Time();
                    break;
                
                case Attack.X2:
                    speed = _values.GetX2Speed();
                    steps = _values.GetX2Time();
                    break;
                
                case Attack.X3:
                    speed = _values.GetX3Speed();
                    steps = _values.GetX3Time();
                    break;
                
                case Attack.Y1:
                    speed = _values.GetY1Speed();
                    steps = _values.GetY1Time();
                    break;
                
                case Attack.Y2:
                    speed = _values.GetY2Speed();
                    steps = _values.GetY2Time();
                    break;
                
                case Attack.Y3:
                    speed = _values.GetY3Speed();
                    steps = _values.GetY3Time();
                    break;
                
                case Attack.B1:
                    speed = _values.GetB1Speed();
                    steps = _values.GetB1Time();
                    break;
                
                case Attack.B2:
                    speed = _values.GetB2Speed();
                    steps = _values.GetB2Time();
                    break;
                
                case Attack.B3:
                    speed = _values.GetB3Speed();
                    steps = _values.GetB3Time();
                    break;
            }
            return ValueTuple.Create(steps, speed);
        }
        

        private void Recoil(float steps, float speed)
        {
            _recoil.SetRecoilValues(steps, speed);
            state.recoilX = true;
        }
        
        private float _attackYValue; 
        private float _attackXValue;
        private float _attackBValue;
        
        private Attack AttackX()
        {
            if (_attackXValue == 0.5f)
                return Attack.X1;

            if (_attackXValue == 1f)
                return Attack.X2;

            return Attack.X3;
        }
        
        private Attack AttackY()
        {
            if (_attackYValue == 0.5f)
                return Attack.Y1;

            if (_attackYValue == 1f)
                return Attack.Y2;

            return Attack.Y3;
        }
        
        private Attack AttackB()
        {
            if (_attackBValue == 0.5f)
                return Attack.B1;

            if (_attackBValue == 1f)
                return Attack.B2;

            return Attack.B3;
        }
        
        

        private void Attack1()
        {
            if (!state.grounded) return;
            if (state.rolling) return;
            
            if (state.canAttack || state.canCancelAttack)
            {
                
                playerChildren.armory.Unarmed();
                
                if (yRotation == 0 && _moveValue > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    _playerAnimationController.Attack1();
                    _playerAnimationController.AttackXValue(CalculateAttackXValue());
                }
                else if (yRotation == 0 && _moveValue < 0)
                {
                    transform.rotation = Quaternion.Euler(0, -180, 0);
                    _playerAnimationController.Attack1();
                    _playerAnimationController.AttackXValue(CalculateAttackXValue());
                }

                else if (Math.Abs(yRotation - (-1)) < 0.05f && _moveValue < 0)
                {
                    transform.rotation = Quaternion.Euler(0, -180, 0);
                    _playerAnimationController.Attack1();
                    _playerAnimationController.AttackXValue(CalculateAttackXValue());
                }
                else if (Math.Abs(yRotation - (-1)) < 0.05f && _moveValue > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    _playerAnimationController.Attack1();
                    _playerAnimationController.AttackXValue(CalculateAttackXValue());
                }
                else
                {
                    _playerAnimationController.Attack1();
                    _playerAnimationController.AttackXValue(CalculateAttackXValue());
                }
            }

            if (!state.recoilX)
            {
                var (speed, steps) = AttackRecoil( AttackX() );
                Recoil(speed, steps);
            }

            
            _rigidbody.velocity = Vector3.zero;
        }



        private void Attack2()
        {
            
            if (!state.grounded) return;
            if (state.rolling) return;

            if (state.canAttack || state.canCancelAttack)
            {
                playerChildren.armory.Unarmed();
                playerChildren.armory.Kunai(true);

                if (yRotation == 0 && _moveValue > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    _playerAnimationController.Attack2();
                    _playerAnimationController.AttackYValue(CalculateAttackYValue());
                }
                else if (yRotation == 0 && _moveValue < 0)
                {
                    transform.rotation = Quaternion.Euler(0, -180, 0);
                    _playerAnimationController.Attack2();
                    _playerAnimationController.AttackYValue(CalculateAttackYValue());
                }

                else if (Math.Abs(yRotation - (-1)) < 0.05f && _moveValue < 0)
                {
                    transform.rotation = Quaternion.Euler(0, -180, 0);
                    _playerAnimationController.Attack2();
                    _playerAnimationController.AttackYValue(CalculateAttackYValue());
                }
                else if (Math.Abs(yRotation - (-1)) < 0.05f && _moveValue > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    _playerAnimationController.Attack2();
                    _playerAnimationController.AttackYValue(CalculateAttackYValue());
                }
                else
                {
                    _playerAnimationController.Attack2();
                    _playerAnimationController.AttackYValue(CalculateAttackYValue());
                }
            }

            if (!state.recoilX)
            {
                var (speed, steps) = AttackRecoil( AttackY() );
                Recoil(speed, steps);
            }


            _rigidbody.velocity = Vector3.zero;
        }

        private void Attack3()
        {
            if (!state.grounded) return;
            if (state.rolling) return;
            
            if (state.canAttack || state.canCancelAttack)
            {
                
                playerChildren.armory.Unarmed();
                playerChildren.armory.Sword();

                if (yRotation == 0 && _moveValue > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    _playerAnimationController.Attack3();
                    _playerAnimationController.AttackBValue(CalculateAttackBValue());
                }
                else if (yRotation == 0 && _moveValue < 0)
                {
                    transform.rotation = Quaternion.Euler(0, -180, 0);
                    _playerAnimationController.Attack3();
                    _playerAnimationController.AttackBValue(CalculateAttackBValue());
                }

                else if (Math.Abs(yRotation - (-1)) < 0.05f && _moveValue < 0)
                {
                    transform.rotation = Quaternion.Euler(0, -180, 0);
                    _playerAnimationController.Attack3();
                    _playerAnimationController.AttackBValue(CalculateAttackBValue());
                }
                else if (Math.Abs(yRotation - (-1)) < 0.05f && _moveValue > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    _playerAnimationController.Attack3();
                    _playerAnimationController.AttackBValue(CalculateAttackBValue());
                }
                else
                {
                    _playerAnimationController.Attack3();
                    _playerAnimationController.AttackBValue(CalculateAttackBValue());
                }
            }

            if (!state.recoilX)
            {
                var (speed, steps) = AttackRecoil( AttackB() );
                Recoil(speed, steps);
            }
            
            _rigidbody.velocity = Vector3.zero;
        }

        
        private void RotateAndMoveCharacter()
        {
            if(state.recoilX)
                return;

            if (!state.walled)
            {
                _rigidbody.velocity = new Vector3(GetMove().normalized.x * moveSpeed, _rigidbody.velocity.y);
            }
            
            if (GetMove().x > 0)
            {
                Transform transform1;
                (transform1 = transform).rotation = Quaternion.Euler(0, 0, 0);
                
            }
            else if (GetMove().x < 0)
            {
                Transform transform1;
                (transform1 = transform).rotation = Quaternion.Euler(0, -180f, 0);

            }
        }
        


        private void Roll()
        {
            _recoil.StopRecoil(state);
            
            gameObject.layer = _playerIgnoreEnemyLayer;

            state.rolling = true;
            
            _playerAnimationController.Roll();

            state.rooted = true;

            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY |
                                     RigidbodyConstraints.FreezePositionZ;
            


            StartCoroutine(SetMaterialBack());
            state.canRoll = false;
            StartCoroutine(RollCalculation());


            if (yRotation == 0 && _moveValue > 0)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
                _rigidbody.AddRelativeForce(new Vector3(rollXValue, 0, 0) * rollVelocity, ForceMode.Impulse);
            }

            else if (yRotation == 0 && _moveValue < 0)
            {
                transform.rotation = Quaternion.Euler(0, -180, 0);
                _rigidbody.AddRelativeForce(new Vector3(-rollXValue, 0, 0) * rollVelocity, ForceMode.Impulse);
            }

            else if (Math.Abs(yRotation - (-1)) < 0.05f && _moveValue < 0)
            {
                transform.rotation = Quaternion.Euler(0, -180, 0);
                _rigidbody.AddRelativeForce(new Vector3(rollXValue, 0, 0) * rollVelocity, ForceMode.Impulse);
            }
            else if (Math.Abs(yRotation - (-1)) < 0.05f && _moveValue > 0)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
                _rigidbody.AddRelativeForce(new Vector3(-rollXValue, 0, 0) * rollVelocity, ForceMode.Impulse);
            }

            else
            {
                _rigidbody.AddRelativeForce(new Vector3(rollXValue, 0, 0) * rollVelocity, ForceMode.Impulse);
            }


            _rigidbody.velocity = Vector3.zero;


            IEnumerator SetMaterialBack()
            {
                yield return new WaitForSeconds(0.2f);
       
                var velocity = _rigidbody.velocity;
                velocity = new Vector3(0, velocity.y, velocity.z);
                _rigidbody.velocity = velocity;

                _rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;

                gameObject.layer = _playerLayer;
                state.rolling = false;
                state.rooted = false;
            }
        }

        IEnumerator RollCalculation()
        {
            yield return new WaitForSeconds(rollCooldown);
            state.canRoll = true;
        }

        private void AirRoll()
        {
            gameObject.layer = _playerIgnoreEnemyLayer;
            state.rolling = true;
            _playerAnimationController.AirRoll();

            state.rooted = false;
            StartCoroutine(SetMaterialBack());
            state.canRoll = false;
            
            StartCoroutine(RollCalculation());
            _rigidbody.AddRelativeForce(airRollDirection * airRollVelocity, ForceMode.Impulse);
            _rigidbody.velocity = Vector3.zero;


            IEnumerator SetMaterialBack()
            {
                yield return new WaitForSeconds(0.2f);
                var velocity = _rigidbody.velocity;
                velocity = new Vector3(0, velocity.y, velocity.z);
                _rigidbody.velocity = velocity;

                state.rolling = false;
                state.rooted = false;
                gameObject.layer = _playerLayer;
            }
        }

        private Vector3 GetMove()
        {
           if (state.rooted) return Vector3.zero;
           return new Vector3(_moveValue, 0);
        }

        private void ResetJumpCounter()
        {
            if (state.grounded)
            {
                _jumpCounter = 0;
            }

        }

        private void JumpCheck()
        {

            if (CanJump() || state.wallSlide)
            {
                if (!state.rolling && !state.fastFalling)
                {
                    Jump();
                }

            }
        }

        private void Jump()
        {
            if (!state.grounded)
            {
                _playerAnimationController.Jump();
            }

            if (!state.rolling)
            {
                state.rooted = false;
            }

            _airTimeCalculator = airTime;
            _rigidbody.velocity = Vector3.zero;
            
            _rigidbody.AddForce(Vector3.up * jumpVelocity, ForceMode.Impulse);
            
            _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, 0f, 0f);
            _jumpCounter += 1;
        }
        
        [SerializeField] private float airTime = 1.0f;
        private float _airTimeCalculator;

        private void ChangePlayerGravity_BasedOnPlayerControls()
        {
            _playerAnimationController.IsSlowFalling(false);
            if (_rigidbody.velocity.y < 0 && _jumpBeingHeld && !state.fastFalling)
            {
                if (_airTimeCalculator > 0)
                {
                    _rigidbody.velocity +=
                        Vector3.up * (Physics.gravity.y * (slowFallVelocity - 1) * Time.fixedDeltaTime);
                    _airTimeCalculator -= Time.deltaTime;
                    _playerAnimationController.IsSlowFalling(true);
                }
                else
                {
                    _rigidbody.velocity +=
                        Vector3.up * (Physics.gravity.y * (fallMultiplier - 1) * Time.fixedDeltaTime);
                }

            }

            else if (_rigidbody.velocity.y < 0)
            {
                _rigidbody.velocity += Vector3.up * (Physics.gravity.y * (fallMultiplier - 1) * Time.fixedDeltaTime);
            }
            else if (_rigidbody.velocity.y > 0 /* && !_jumpBeingHeld */)
            {
                _rigidbody.velocity += Vector3.up * (Physics.gravity.y * (lowJumpMultiplier - 1) * Time.fixedDeltaTime);
            }
        }

        private bool CanJump()
        {
            return _jumpCounter < numberOfExtraJumps;
        }

        public void SetCanCancel(bool attack)
        {
            state.canCancelAttack = attack;
        }

    }

}
