﻿using System;
using System.Collections;
using System.Collections.Generic;
using __Game.Player.Scripts;
using UnityEngine;

public class Armory : MonoBehaviour
{
    [SerializeField] private GameObject swordL;
    [SerializeField] private GameObject swordR;
    [SerializeField] private GameObject kunaiL;
    [SerializeField] private GameObject kunaiR;

    [SerializeField] private PlayerState _playerState;
    private void Start()
    {
        Unarmed();
    }

    public void Unarmed()
    {
        swordL.SetActive(false);
        swordR.SetActive(false);
        kunaiL.SetActive(false);
        kunaiR.SetActive(false);
    }

    public void Sword()
    {
        if (!_playerState.facingRight)
        {
            SwordL(true);
            SwordR(false);
        }

        else
        {
            SwordR(true);
            SwordL(false);
        }
    }

    public void SwordL(bool active)
    {
        swordL.SetActive(active);
    }

    public void SwordR(bool active)
    {
        swordR.SetActive(active);
    }

    public void Kunai(bool active)
    {
        kunaiL.SetActive(active);
        kunaiR.SetActive(active);
    }
}
