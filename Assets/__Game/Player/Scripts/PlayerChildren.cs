﻿using System;
using System.Collections;
using System.Collections.Generic;
using __Game.Player.Scripts;
using UnityEngine;

public class PlayerChildren : MonoBehaviour
{
    [SerializeField] private PlayerState playerState;

    [SerializeField] private LayerMask ground;
    [SerializeField] private LayerMask enemy;
    [SerializeField] private LayerMask wall;
    
    [SerializeField] private float groundDistance = 0.4f;
    
    [SerializeField] private Transform _groundChecker;
    [SerializeField] private Transform _wallChecker;
    
    [SerializeField] public Armory armory;
    
    public GameObject graphics;
    
    public GameObject attackPointX1;
    public GameObject attackPointX2;
    public GameObject attackPointX3;

    public GameObject attackPointY1;
    public GameObject attackPointY2;
    public GameObject attackPointY3;

    public GameObject attackPointB1;
    public GameObject attackPointB2;
    public GameObject attackPointB3;
    
    private void ProcessGrounded()
    {
        var position = _groundChecker.position;
        playerState.grounded = Physics.CheckSphere(position, groundDistance, ground, QueryTriggerInteraction.Ignore);
        //|| Physics.CheckSphere(position, groundDistance, enemy, QueryTriggerInteraction.Ignore);
    }
    
    private void ProcessWalled()
    {
        playerState.walled = Physics.CheckSphere(_wallChecker.position, groundDistance, wall, QueryTriggerInteraction.Ignore);
    }

    private void Update()
    {
        ProcessGrounded();
        ProcessWalled();
    }
}
