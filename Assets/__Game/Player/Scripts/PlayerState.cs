﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace __Game.Player.Scripts
{
    public class PlayerState : MonoBehaviour
    {
        public bool jumping;
        public bool falling;
        public bool slowFalling;
        public bool fastFalling;

        
        public bool grounded;
        public bool walled;
        public bool roofed;
        
        
        public bool wallSlide;
        public bool rolling;
        public bool airRolling;
        public bool canRoll;

        public bool attacking;
        public bool canAttack;
        public bool canCancelAttack;
        
        
        public bool running;
        public bool rooted;
        public bool stunned;
        public bool dead;
    
    
        public bool atInteractable;
        public bool interacting;
        public bool facingRight;
    
    
        public bool recoilX;
        public bool recoilY;

        public bool encounter;
        public bool flanked;
        
    }
}

