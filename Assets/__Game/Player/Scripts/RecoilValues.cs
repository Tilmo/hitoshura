﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecoilValues : MonoBehaviour
{
    [SerializeField] private float x1Time;
    [SerializeField] private float x1Speed;
    
    [SerializeField] private float x2Time;
    [SerializeField] private float x2Speed;
    
    [SerializeField] private float x3Time;
    [SerializeField] private float x3Speed;
    
    
    [SerializeField] private float y1Time;
    [SerializeField] private float y1Speed;
    
    [SerializeField] private float y2Time;
    [SerializeField] private float y2Speed;
    
    [SerializeField] private float y3Time;
    [SerializeField] private float y3Speed;
    
    
    [SerializeField] private float b1Time;
    [SerializeField] private float b1Speed;
    
    [SerializeField] private float b2Time;
    [SerializeField] private float b2Speed;
    
    [SerializeField] private float b3Time;
    [SerializeField] private float b3Speed;

    public float GetX1Time() { return x1Time; }
    public float GetX1Speed() { return x1Speed; }

    public float GetX2Time()
    {
        return x2Time;
    }
    public float GetX2Speed()
    {
        return x2Speed;
    }
    public float GetX3Speed()
    {
        return x3Speed;
    }
    public float GetX3Time()
    {
        return x3Time;
    }

    public float GetY1Time()
    {
        return y1Time;
    }
    public float GetY2Time()
    {
        return y2Time;
    }
    public float GetY3Time()
    {
        return y3Time;
    }

    public float GetY1Speed()
    {
        return y1Speed;
    }
    public float GetY2Speed()
    {
        return y2Speed;
    }
    public float GetY3Speed()
    {
        return y3Speed;
    }

    public float GetB1Time()
    {
        return b1Time;
    }
    public float GetB2Time()
    {
        return b2Time;
    }
    public float GetB3Time()
    {
        return b3Time;
    }

    public float GetB1Speed()
    {
        return b1Speed;
    }
    public float GetB2Speed()
    {
        return b2Speed;
    }
    public float GetB3Speed()
    {
        return b3Speed;
    }


}
