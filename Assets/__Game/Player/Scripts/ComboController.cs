﻿using System;
using System.Collections;
using System.Collections.Generic;
using __Game.Enemy.Scripts;
using TMPro;
using UnityEngine;

public class ComboController : MonoBehaviour
{
    [SerializeField] private int comboCounter;
    [SerializeField] private int score;
    [SerializeField] private float timer = 0;
    [SerializeField] private float timerFloat;
    [SerializeField] private float scoreMultiplier;
    [SerializeField] private Rank combo;

    [SerializeField] private ScoreSystemUI scoreSystemUI;

    private TextMeshProUGUI _scoreUI;
    private TextMeshProUGUI _rankUI;
    private TextMeshProUGUI _comboCounterUI;

    [SerializeField] private bool combat;

    private void Start()
    {
        _scoreUI = scoreSystemUI.GetScoreUI();
        _rankUI = scoreSystemUI.GetRankUI();
        _comboCounterUI = scoreSystemUI.GetComboCounterUI();
    }

    private bool _updateUI;
    private enum Rank
    {
        S,
        A,
        B,
        C, 
        D
    };
    
    private void OnEnable()
    {
        EnemyDamageController.HitCombo += ComboCounter;
        PlayerHealth.BreakCombo += TakeDamage;
    }

    public void TakeDamage()
    {
        AddScore();
        combat = false;
        timer = 0;
    }

    public void DisableUI(bool active)
    {
        _updateUI = active;
    }

    private void InvisibleUI()
    {
        _rankUI.text = "";
        _comboCounterUI.text = "";
    }

    private void UpdateUI()
    {
        _rankUI.text = RankString();
        _comboCounterUI.text = comboCounter.ToString();
    }
    private void ComboCounter()
    {
        combat = true;
        timer = timerFloat;
        comboCounter++;
    }

    private void FixedUpdate()
    {
        combo = CheckRank();
        scoreMultiplier = ScoreMultiplier();

        if (score < _realScore)
            score += AddingSpeed();
        
        if (combat)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                combat = false;
                AddScore();
            }
        }

        _scoreUI.text = score.ToString();
        
        if(_updateUI) 
            UpdateUI();
        else InvisibleUI();
        
    }

    [SerializeField] private int _realScore;
    private void AddScore()
    {
        _realScore += ( (comboCounter * 100) * ScoreMultiplier());
        //score += ( (comboCounter * 100) * ScoreMultiplier());
        Reset();
    }
    

    private void Reset()
    {
        comboCounter = 0;
        combo = Rank.D;
    }

    private int ScoreMultiplier()
    {
        switch (combo)
        {
            case Rank.S:
                return 5;
            
            case Rank.A:
                return 4;
            
            case Rank.B:
                return 3;
            
            case Rank.C:
                return 2;
            
            default:
            {
                return 1;
            }
        }
    }

    private Rank CheckRank()
    {
        if (comboCounter > 19)
            return Rank.S;

        if (comboCounter > 14)
            return Rank.A;

        if (comboCounter > 9)
            return Rank.B;

        return comboCounter > 4 ? Rank.C : Rank.D;
    }

    public float GetTimerFloat()
    {
        return timerFloat;
    }

    public float GetTimer()
    {
        return timer;
    }


    private string RankString()
    {
        switch (combo)
        {
            case Rank.S:
                return "S";
            case Rank.A:
                return "A";
            case Rank.B:
                return "B";
            case Rank.C:
                return "C";
            case Rank.D:
                return "D";
            default:
                return "";
        }
    }

    private int AddingSpeed()
    {
        var i = _realScore - score;

        if (i > 2000)
            return 100;
        
        return 10;
        
        
    } 
}
