﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class EncounterCamera : MonoBehaviour
{
    private GameObject encounterCamera;

    private void Start()
    {
        encounterCamera = GameObject.FindGameObjectWithTag("EncounterCamera");
    }

    public void Activate()
    {
        encounterCamera.SetActive(true);
    }

    public void Deactivate()
    {
        encounterCamera.SetActive(false);
    }

}
