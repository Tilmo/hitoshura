﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderBar : MonoBehaviour
{
    public Slider slider;
    
    public void SetValue(float health)
    {
        slider.value = health;
    }

    public void SetMaxValue(float maxHealth)
    {
        slider.maxValue = maxHealth;
    }
    
    
    public void Deactivate(bool deactivate)
    {
        slider.gameObject.SetActive(deactivate);
    }
}
