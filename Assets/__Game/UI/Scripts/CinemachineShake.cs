﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CinemachineShake : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera encounterCamera;
    private float _shakeTimer;
    public static CinemachineShake instance { get; private set; }

    private void Awake()
    {
        instance = this;
    }

    public void ShakeCamera(float intensity, float time)
    {
        var cinemachineBasicMultiChannelPerlin =
            encounterCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = intensity;
        _shakeTimer = time;
    }

    private void Update()
    {
        if (_shakeTimer > 0)
        {
            _shakeTimer -= Time.deltaTime;
        }
        else
        {
            var cinemachineBasicMultiChannelPerlin =
                encounterCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

            cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = 0f;
        }
    }
}
