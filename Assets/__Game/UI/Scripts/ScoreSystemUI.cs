﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreSystemUI : MonoBehaviour
{
  [SerializeField] private TextMeshProUGUI scoreUI;
  [SerializeField] private TextMeshProUGUI rankUI;
  [SerializeField] private TextMeshProUGUI comboCounterUI;

  public TextMeshProUGUI GetScoreUI()
  {
    return scoreUI;
  }
  
  public TextMeshProUGUI GetRankUI()
  {
    return rankUI;
  }
  
  public TextMeshProUGUI GetComboCounterUI()
  {
    return comboCounterUI;
  }
}
