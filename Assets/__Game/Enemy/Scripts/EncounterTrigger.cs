﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterTrigger : MonoBehaviour
{
    private EncounterController _encounterController;

    private void Start()
    {
        _encounterController = GetComponentInParent<EncounterController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _encounterController.StartEncounter();
            gameObject.SetActive(false);
        }
        
    }
}
