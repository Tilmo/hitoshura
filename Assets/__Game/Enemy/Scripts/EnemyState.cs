﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyState : MonoBehaviour
{
    public bool recoilX;
    public bool facingRight;
    public bool stunned;
    public bool dead;
    public bool engaged;
    public bool blocked;
    public bool attacking;
    public bool wallSensor;
    public bool canAttack;

    public bool backTouchingWall;
    public bool frontTouchingWall;

    public bool canTurn;
    public bool attackAnimation;

    public bool charge;

}
