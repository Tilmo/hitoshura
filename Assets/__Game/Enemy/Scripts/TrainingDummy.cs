﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingDummy : MonoBehaviour
{

    public EnemyState enemyState;
    public Recoil recoil;

    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        recoil.ProcessRecoilTimer(enemyState);
        recoil.ProcessRecoil(enemyState, _rigidbody);

        if (!enemyState.recoilX)
            _rigidbody.velocity = Vector3.zero;
    }
    

    public void Recoil()
    {
        enemyState.recoilX = true;
    }
}
