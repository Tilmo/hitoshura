﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollision : MonoBehaviour
{
    private EnemyState _enemyState;
    private Animator _animator;

    private void Start()
    {
        _enemyState = GetComponent<EnemyState>();
        _animator = GetComponentInChildren<Animator>();

    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            if (_enemyState.recoilX)
            {
                _enemyState.recoilX = false;
                _animator.SetTrigger("Wallhit");
            }
        }
    }
}
