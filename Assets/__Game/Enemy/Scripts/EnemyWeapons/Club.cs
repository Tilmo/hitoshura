﻿using System.Collections;
using System.Collections.Generic;
using __Game.Player.Scripts;
using UnityEngine;

public class Club : MonoBehaviour
{
    [SerializeField] private int damage;

    [SerializeField] private EnemyState state;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !state.attackAnimation)
        {
            other.GetComponent<PlayerController>().TakeDamage(damage);
        }
        
    }
}
