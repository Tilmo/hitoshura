﻿using System;
using System.Collections;
using System.Collections.Generic;
using __Game.Player.Scripts;
using UnityEngine;

public class EnemySword : MonoBehaviour
{
    public enum Hand
    {
        Left,
        Right
    };
    
    public Hand weaponHand;
    
    [SerializeField] private int damage;
    private EnemyState _state;

    private void OnEnable()
    {
        _state = GetComponentInParent<EnemyState>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && _state.attacking && HandCheck())
        {
            other.GetComponent<PlayerController>().TakeDamage(damage);
        }
    }

    private bool HandCheck()
    {
        if (weaponHand == Hand.Right)
        {
            if (_state.facingRight)
            {
                return false;
            }

            else
            {
                return true;
            }
        }

        else
        {
            if (_state.facingRight)
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }
}
