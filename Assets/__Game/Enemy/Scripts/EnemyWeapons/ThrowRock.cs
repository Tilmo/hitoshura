﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowRock : MonoBehaviour
{
  [SerializeField] private GameObject rockPrefab;
  [SerializeField] private GameObject rockSpawnPos;
  [SerializeField] public GameObject target;
  [SerializeField] private float speed;
  [SerializeField] private float turnSpeed = 2f;
  [SerializeField] private GameObject parent;

  [SerializeField] private float fire;

  public bool inRange;

  private void Start()
  {
    target = GameObject.FindGameObjectWithTag("Player");
  }

  private void Update()
  {
    Vector3 direction = (target.transform.position - parent.transform.position).normalized;
    Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
    parent.transform.rotation = Quaternion.Slerp(parent.transform.rotation, lookRotation, Time.deltaTime * turnSpeed);

    float? angle = RotateTurret();

    inRange = angle != null && Vector3.Angle(direction, parent.transform.forward) < 5;
  }

  public void SetOffset(float x)
  {
    _offset = new Vector3(x, 0, 0);
  }

  public void Shoot()
  {
    GameObject rock = Instantiate(rockPrefab, rockSpawnPos.transform.position, rockSpawnPos.transform.rotation);
    rock.GetComponent<Rigidbody>().velocity = speed * transform.forward;
  }

  private Vector3 _offset;

  private float? CalculateAngle(bool low)
  {
    Vector3 targetDir = (target.transform.position + _offset ) -parent.transform.position;
    float y = targetDir.y;
    targetDir.y = 0f;
    float x = targetDir.magnitude;
    float gravity = 9.81f;
    float sSqr = speed * speed;
    float underTheSqrRoot = (sSqr * sSqr) - gravity * (gravity * x * x + 2 * y * sSqr);

    if (underTheSqrRoot >= 0f)
    {
      float root = Mathf.Sqrt(underTheSqrRoot);
      float highAngle = sSqr + root;
      float lowAngle = sSqr - root;

      if (low)
        return (Mathf.Atan2(lowAngle, gravity * x) * Mathf.Rad2Deg);

      else
      {
        return (Mathf.Atan2(highAngle, gravity * x) * Mathf.Rad2Deg);
      }
    }
    else
    {
      return null;
    }
  }

  private float? RotateTurret()
  {
    float? angle = CalculateAngle(true);
    if (angle != null)
    {
      this.transform.localEulerAngles = new Vector3(360f - (float) angle, 0f, 0f);
    }

    return angle;
  }
}
