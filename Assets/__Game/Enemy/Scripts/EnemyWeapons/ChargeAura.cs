﻿using System;
using System.Collections;
using System.Collections.Generic;
using __Game.Player.Scripts;
using UnityEngine;

public class ChargeAura : MonoBehaviour
{
    [SerializeField] private int damage;

    private EnemyState _enemyState;

    private void Start()
    {
        _enemyState = GetComponentInParent<EnemyState>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && _enemyState.charge)
        {
            other.GetComponent<PlayerController>().TakeDamage(damage);
        }
        
    }
}
