﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TenguBossTornado : MonoBehaviour
{
    private Vector3 _target;
    private Rigidbody _rigidbody;
    private float _timer;
    private int _direction;

    private enum FireSequence
    {
        Start,
        Wait,
        Fire
    };

    private FireSequence _current;

    private void OnEnable()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void StartFireSequence(Vector3 position)
    {
        _rigidbody.velocity = 0.5f * 2f * (_target-position);
        _current = FireSequence.Start;
    }
    public void SetTarget(Vector3 target, int direction)
    {
        _target = target;
        _direction = direction;
    }
    
    

    private void FixedUpdate()
    {
        if (_current == FireSequence.Start)
        {
            if (Vector3.Distance(transform.position, _target) < 0.1f)
            {
                _timer = _chargeUpTime;
                _current = FireSequence.Wait;
            }
        }

        if (_current == FireSequence.Wait)
        {
            if (_timer > 0)
            {
                _timer -= Time.deltaTime;
                _rigidbody.velocity = Vector3.zero;
            }

            else
            {
                _current = FireSequence.Fire;
            }
        }

        if (_current == FireSequence.Fire)
        {
            _rigidbody.velocity = Vector3.right * (_speed * _direction);
        }
    }

    private float _speed;
    private float _chargeUpTime;

    public void SetSpeed(float speed, float charge)
    {
        _speed = speed;
        _chargeUpTime = charge;
    }
}
