﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disengage : MonoBehaviour
{
    private EnemyState _enemyState;

    private void Start()
    {
        _enemyState = GetComponentInParent<EnemyState>();
    }

    private void FixedUpdate()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.right, out hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, (transform.right) * hit.distance, Color.red);
            
            if (hit.collider.gameObject.CompareTag("EncounterWall") && hit.distance < 1)
            {
                _enemyState.wallSensor = true;
            }
            else _enemyState.wallSensor = false;
        }
    }
}
