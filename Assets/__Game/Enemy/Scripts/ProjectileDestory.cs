﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDestory : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("Enemy"))
            return;
        Destroy(gameObject);
    }
    
}
