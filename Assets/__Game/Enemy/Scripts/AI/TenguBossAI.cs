﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class TenguBossAI : MonoBehaviour
{
    private Animator _animator;
    private GameObject _player;
    private NavMeshAgent _agent;
    public EnemyState enemyState;
    public GameObject tornado;

    public bool flying;

    private int _directionModifier;
    
    [SerializeField] private GameObject leftPerch;
    [SerializeField] private GameObject rightPerch;

    [SerializeField] private List<GameObject> tornadoPatternMarkers;
    [SerializeField] private List<GameObject> tornadoPatternMarkers2;
    [SerializeField] private List<GameObject> tornadoPatternMarkers3;

    private enum Position
    {
        Left, Right
    }

    [SerializeField] private Position currentPosition;
    
    private enum Stage
    {
        Enter,
        Update,
        Exit
    }

    private enum State
    {
        Idle,
        Disengage,
    }
    [SerializeField] private Stage stage;
    [SerializeField] private State state;


    private List<GameObject> RandomList()
    {
        var i = Random.Range(0, 3);
        if (i == 0)
            return tornadoPatternMarkers;
        if (i == 1)
            return tornadoPatternMarkers2;
        if (i == 2)
            return tornadoPatternMarkers3;

        return null;
    }

    private bool _canProcess = true;

    private void Start()
    {
        stage = Stage.Enter;
        state = State.Idle;

        _animator = GetComponentInChildren<Animator>();
        _player = GameObject.FindGameObjectWithTag("Player");
        _agent = GetComponent<NavMeshAgent>();
        enemyState = GetComponent<EnemyState>();

        if (currentPosition == Position.Left)
        {
            RoostLeft();
        }
        else
        {
            RoostRight();
        }
    }

    private void RoostRight()
    {
        _agent.transform.position = rightPerch.transform.position;
        transform.rotation = Quaternion.Euler(0, 180, 0);
        enemyState.facingRight = false;
        currentPosition = Position.Right;
        _directionModifier = -1;
    }

    private void RoostLeft()
    {
        _agent.transform.position = leftPerch.transform.position;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        enemyState.facingRight = true;
        currentPosition = Position.Left;
        _directionModifier = 1;
    }

    private Vector3 Direction()
    {
        return _player.transform.position - transform.position;
    }

    private void ProcessRoosting()
    {
        _animator.SetTrigger("Idle");
        attackTimer = 0;
        
        if(currentPosition == Position.Right)
            RoostLeft();
        else if (currentPosition == Position.Left)
            RoostRight();
    }

    private void FixedUpdate()
    {
        if (enemyState.dead)
        {
            flying = false;
            _canProcess = false;
            _animator.SetTrigger("Die");
            enemyState.dead = false;
            Invoke(nameof(Die), 1f);
        }
    }
    private void Die()
    {
        gameObject.SetActive(false);
    }

    private void Update()
    {
        if (!_canProcess) return;
        
        else
        {
            if(state == State.Idle)
                Idle();
            
            if(state == State.Disengage)
                Disengage();
        }
        
    }

    private float _timer;
    private Vector3 _positionSnapshot;

    private void Teleport()
    {
        ProcessRoosting();
    }
    
    [SerializeField] private float attackTimer;
    [SerializeField] private float attackCooldown;
    private void Idle()
    {
        if(!_canProcess) return;
        
        if (stage == Stage.Enter)
        {
            _positionSnapshot = _player.transform.position;
            
            _timer = 0.5f;
            stage = Stage.Update;
        }

        if (stage == Stage.Update)
        {
            if (Vector3.Distance(transform.position, _player.transform.position) < 8)
            {
                state = State.Disengage;
                stage = Stage.Exit;
            }
            
            else if (_timer > 0)
            {
                _timer -= Time.deltaTime;
                
                if (attackTimer > 0)
                    attackTimer -= Time.deltaTime;
                else
                {
                    _animator.SetTrigger("Attack");
                    TornadoShotgun();
                    attackTimer = attackCooldown;
                }
            }
            
            else
            {
                stage = Stage.Exit;
            }
        }
        
        if (stage == Stage.Exit)
        {
            stage = Stage.Enter;
        }
    }
    private void Disengage()
    {
        if (stage == Stage.Enter)
        {
            _animator.SetTrigger("TakeOff");
            _animator.SetFloat("TakeOffTime", (1 / (vulnerabilityTime-1 )));
            _timer = vulnerabilityTime+1f;
            stage = Stage.Update;
        }

        if (stage == Stage.Update)
        {
            if (_timer > 0)
            {
                _timer -= Time.deltaTime;
            }
            
            else 
            {
                Teleport();
                stage = Stage.Exit;
            }
        }

        if (stage == Stage.Exit)
        {
            stage = Stage.Enter;
            state = State.Idle;
        }
    }

    [SerializeField] private float projectileSpeed;
    [SerializeField] private float vulnerabilityTime;
    [SerializeField] private float tornadoChargeUpTime;
    
    public void TornadoShotgun()
    {
        foreach (var test in RandomList())
        {
            var transform1 = transform;
            var position = transform1.position;
            var projectile = Instantiate(tornado, position, transform1.rotation);
            var tenguBossTornado = projectile.GetComponent<TenguBossTornado>();
            tenguBossTornado.SetSpeed(projectileSpeed, tornadoChargeUpTime);
            tenguBossTornado.SetTarget(test.transform.position, _directionModifier);
            tenguBossTornado.StartFireSequence(position);
        }
    }
}
