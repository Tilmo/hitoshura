﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class TenguRockThrowerAI : MonoBehaviour
{
    private Animator _animator;
    private GameObject _player;
    private NavMeshAgent _agent;
    private HiveMind _hiveMind;
    public EnemyState enemyState;
    private Recoil _recoil;
    public GameObject tornado;
    private ThrowRock _throwRock;
    [SerializeField] private float attackLag;
    
    private bool _canProcess = true;
    
    private enum Stage
    {
        Enter,
        Update,
        Exit
    }

    private enum State
    {
        Idle,
    }
    [SerializeField] private Stage stage;
    [SerializeField] private State state;
    

    private void Start()
    {
        stage = Stage.Enter;
        state = State.Idle;

        _animator = GetComponentInChildren<Animator>();
        _player = GameObject.FindGameObjectWithTag("Player");
        _agent = GetComponent<NavMeshAgent>();
        _hiveMind = GetComponentInParent<HiveMind>();
        enemyState = GetComponent<EnemyState>();
        _recoil = GetComponent<Recoil>();
        _throwRock = GetComponentInChildren<ThrowRock>();
    }

    private Vector3 Direction()
    {
        return _player.transform.position - transform.position;
    }

    private void FixedUpdate()
    {
        _recoil.ProcessRecoilTimer(enemyState);
        
        
        if (enemyState.dead)
        {
            _canProcess = false;
            _animator.SetTrigger("Die");
            enemyState.dead = false;
            Invoke(nameof(Die), 1f);
        }
    }
    
    private void Die()
    {
        gameObject.SetActive(false);
    }

    private void Update()
    {
        _recoil.ProcessRecoil(enemyState, _agent);
        if (!_canProcess) return;
        
        if(Direction().x < 0)
            _recoil.SetDirectionModifier(Vector3.right);
        else _recoil.SetDirectionModifier(Vector3.left);

        if(enemyState.stunned)
            _animator.SetTrigger("Stun");
        
        else
        {
            if(state == State.Idle)
                Idle();
        }
        
    }

    private float _timer;
    private float _attackTimer;
    private Vector3 _positionSnapshot;
    
    private void Idle()
    {
        if (stage == Stage.Enter)
        {
            _positionSnapshot = _player.transform.position;

            _timer = 0.5f;
            _attackTimer = 1f;
            
            stage = Stage.Update;
        }

        if (stage == Stage.Update)
        {
            if (_attackTimer > 0)
            {
                _attackTimer -= Time.deltaTime;
            }
            if (_timer > 0)
            {
                _timer -= Time.deltaTime;
            }
            else
            {
                if (!_animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
                {
                    if ((_player.transform.position - transform.position).x > 0)
                    {
                        transform.rotation = Quaternion.Euler(0, 180, 0);
                        enemyState.facingRight = true;
                    }
                    
                    else if ((_player.transform.position - transform.position).x < 0)
                    {
                        transform.rotation = Quaternion.Euler(0, 0, 0);
                        enemyState.facingRight = false;
                    }
                }
                
                if (_hiveMind.CanAttack(5) && !enemyState.canAttack)
                {
                    _hiveMind.AddEncounterValue(5);
                    _hiveMind.ReturnToken(5);
                    StartCoroutine(ThrowRockCooldown(attackLag));
                    _throwRock.SetOffset(throwOffsetX);

                    _animator.SetTrigger("Attack");
                }
                stage = Stage.Exit;
            }
        }
        
        if (stage == Stage.Exit)
        {
            stage = Stage.Enter;
        }
    }

    public void ThrowRock()
    {
        _throwRock.Shoot();
    }

    [SerializeField] private float throwOffsetX;

    IEnumerator ThrowRockCooldown(float lag)
    {
        enemyState.canAttack = !enemyState.canAttack;
        yield return new WaitForSeconds(lag);
        enemyState.canAttack = !enemyState.canAttack;
    }

}

