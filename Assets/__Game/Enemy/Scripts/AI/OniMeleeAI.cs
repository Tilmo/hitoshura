﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class OniMeleeAI : MonoBehaviour
{
    [SerializeField] private EnemyState enemyState;
    [SerializeField] private OniAnimations animations;
    [SerializeField] private OniWeaponry weaponry;

    [SerializeField] private NavMeshAgent agent;
    
    private float _moveTimer;
    private float _moveSpeed;
    private float _attackTimer;

    [SerializeField] private float walkSpeed;
    
    private HiveMind _hiveMind;
    private Recoil _recoil;

    private GameObject _player;
    private bool _canProcess = true;
    private float _timer;
    private Vector3 _positionSnapshot;
    
    [SerializeField] private float attackCooldown;
    [SerializeField] private bool playerComingTowards;
    [SerializeField] private bool playerRunningAway;
    [SerializeField] private bool playerStanding;

    [SerializeField] private Stage stage = Stage.Enter;
    private enum Stage
    {
        Enter,
        Update,
        Exit
    }

    [SerializeField] private State state = State.Idle;
    private enum State
    {
        Idle, Walk, Attack
    }

    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _recoil = GetComponent<Recoil>();
        _hiveMind = GetComponentInParent<HiveMind>();
    }

    private void FixedUpdate()
    {
        PlayerBooleans();
        Move();
        AttackTimer();
        
        if (enemyState.stunned)
        {
            stage = Stage.Enter;
            state = State.Idle;
        }
        
        _recoil.ProcessRecoilTimer(enemyState);
        _recoil.ProcessRecoil(enemyState, agent);
        
        if (enemyState.dead)
        {
            _canProcess = false;
            enemyState.dead = false;
            animations.Die();
            Invoke(nameof(Die), 1f);
        }
    }

    private void Update()
    {
        if (!_canProcess) return;
        
        else
        {
            if(state == State.Idle)
                Idle();
        
            if(state == State.Attack)
                Attack();
        
            if(state == State.Walk)
                Walk();
  
        }
    }

    private void Idle()
    {
        if (stage == Stage.Enter)
        { 
            animations.Idle();
            _positionSnapshot = _player.transform.position;
            _timer = 0.25f;

            
            
            stage = Stage.Update;
        }

        if (stage == Stage.Update)
        {
            if (_timer > 0) { _timer -= Time.deltaTime; }
            
            else
            {
                RotateTowardsPlayer();

                
                if (Vector3.Distance(transform.position, _positionSnapshot) < 5f && enemyState.canAttack)
                {
                    _attackTimer = attackCooldown;
                    enemyState.canAttack = false;
                    state = State.Attack;
                }
                
                
                else if (Vector3.Distance(transform.position, _positionSnapshot) < 15f && enemyState.canTurn)
                {
                    state = State.Walk;
                }
                
                
                
                stage = Stage.Exit;
            }
        }

        if (stage == Stage.Exit)
        {
            animations.ResetIdle();
            
            
            
            stage = Stage.Enter;
        }
    }

    private void Attack()
    {
        if (stage == Stage.Enter)
        {
            _timer = 0.1f;
            
            
            
            stage = Stage.Update;
        }

        if (stage == Stage.Update)
        {
            if(_timer > 0)
            {
                _timer -= Time.deltaTime;
            }

            else
            {
                
                animations.Attack();

                _moveSpeed = 2.5f;
                _moveTimer = 0.4f;
                
                
                
                stage = Stage.Exit;
            }
        }

        if (stage == Stage.Exit)
        {
            
            
            
            state = State.Idle;
            stage = Stage.Enter;
        }
    }

    private void Walk()
    {
        if (stage == Stage.Enter)
        {
            animations.Walk();
            animations.WalkMultiplier(1f);
            _timer = 3f;

            
            
            stage = Stage.Update;
        }

        if (stage == Stage.Update)
        {
            if (_timer > 0)
            {
                _timer -= Time.deltaTime;
                RotateTowardsPlayer();


                if (playerRunningAway)
                {
                    agent.Move( (_player.transform.position-transform.position).normalized * (Time.deltaTime * walkSpeed * 2));
                    animations.WalkMultiplier(2);
                }

                else
                {
                    agent.Move( (_player.transform.position-transform.position).normalized * (Time.deltaTime * walkSpeed));
                }
 

                if (Vector3.Distance(transform.position, _player.transform.position) < 3)
                {
                    
                    
                    
                    stage = Stage.Exit;
                }
            }
            else
            {
                
                
                
                stage = Stage.Exit;
            }
       
        }

        if (stage == Stage.Exit)
        {
            animations.ResetWalk();
            
            
            
            state = State.Idle;
            stage = Stage.Enter;
        }
    }

    private void Move()
    {
        if (_moveTimer > 0)
        {
            _moveTimer -= Time.deltaTime;

            if (!enemyState.stunned)
            {
                agent.Move(-transform.right * (_moveSpeed * Time.deltaTime));
            }
        }
    }


    private void AttackTimer()
    {
        if (_attackTimer > 0)
        {
            _attackTimer -= Time.deltaTime;
        }
        else
        {
            enemyState.canAttack = true;
        }
    }
    

    private void Die()
    {
        gameObject.SetActive(false);
    }
    
    
    private Vector3 Direction() { return _player.transform.position - transform.position; }

    private void RotateTowardsPlayer()
    {
        if (enemyState.canTurn)
        {
            if ((_player.transform.position - transform.position).x > 0)
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
                enemyState.facingRight = true;
                
                //weaponry.ClubL();
                weaponry.WeaponL();
            }
                    
            else if ((_player.transform.position - transform.position).x < 0)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
                enemyState.facingRight = false;
                //weaponry.ClubR();
                weaponry.WeaponR();
            }
        }
        else
        {
            return;
        }
    }

    private void PlayerBooleans()
    {
        var position = transform.position;
        var playerPosition = _player.transform.position;
        playerStanding = Math.Abs(Vector3.Distance(position, _positionSnapshot) -
                                   (Vector3.Distance(position, playerPosition))) < 0.1f;
        playerComingTowards = Vector3.Distance(position, _positionSnapshot) >
                               (Vector3.Distance(position, playerPosition));
        playerRunningAway = Vector3.Distance(position, _positionSnapshot) <
                             (Vector3.Distance(position, playerPosition));
    }

}
