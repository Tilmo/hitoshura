﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class TenguRangedAeromancerAI : MonoBehaviour
{
    private Animator _animator;
    private GameObject _player;
    private NavMeshAgent _agent;
    private HiveMind _hiveMind;
    public EnemyState _enemyState;
    private Recoil _recoil;
    public GameObject tornado;
    public bool facingRight;
    private bool _canProcess = true;
    
    private enum Stage
    {
        Enter,
        Update,
        Exit
    }

    private enum State
    {
        Idle,
        Attack,
        Disengage,
    }
    [SerializeField] private Stage stage;
    [SerializeField] private State state;
    

    private void Start()
    {
        stage = Stage.Enter;
        state = State.Idle;

        _animator = GetComponentInChildren<Animator>();
        _player = GameObject.FindGameObjectWithTag("Player");
        _agent = GetComponent<NavMeshAgent>();
        _hiveMind = GetComponentInParent<HiveMind>();
        _enemyState = GetComponent<EnemyState>();
        _recoil = GetComponent<Recoil>();
    }

    private Vector3 Direction()
    {
        return _player.transform.position - transform.position;
    }

    private void FixedUpdate()
    {
        _recoil.ProcessRecoilTimer(_enemyState);
        
        if (_enemyState.dead)
        {
            _canProcess = false;
            _animator.SetTrigger("Die");
            _enemyState.dead = false;
            Invoke(nameof(Die), 1f);
        }
    }
    
    private void Die()
    {
        gameObject.SetActive(false);
    }

    private void Update()
    {
        _recoil.ProcessRecoil(_enemyState, _agent);
        if (!_canProcess) return;
        
        if(Direction().x < 0)
            _recoil.SetDirectionModifier(Vector3.right);
        else _recoil.SetDirectionModifier(Vector3.left);
        

        if (_enemyState.dead)
        {
            gameObject.SetActive(false);
            _enemyState.dead = false;
        }

        else if(_enemyState.stunned)
            _animator.SetTrigger("Stun");
        
        else
        {
            if(state == State.Idle)
                Idle();
            
            if(state == State.Disengage)
                Disengage();
        }

        debugDistance = Vector3.Distance(transform.position, _positionSnapshot);
    }

    private float _timer;
    private float _attackTimer;
    private Vector3 _positionSnapshot;

    [SerializeField] private float debugDistance;

    private bool _disengage = true;
    private void Idle()
    {
        if (stage == Stage.Enter)
        {
            _positionSnapshot = _player.transform.position;

            _timer = 0.5f;

            stage = Stage.Update;
        }

        if (stage == Stage.Update)
        {
            if (_attackTimer > 0)
            {
                _attackTimer -= Time.deltaTime;
            }
            if (_timer > 0)
            {
                _timer -= Time.deltaTime;
            }
            else
            {
                if (!_animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
                {
                    if ((_player.transform.position - transform.position).x > 0)
                    {
                        transform.rotation = Quaternion.Euler(0, 180, 0);
                        _enemyState.facingRight = true;
                    }
                    
                    else if ((_player.transform.position - transform.position).x < 0)
                    {
                        transform.rotation = Quaternion.Euler(0, 0, 0);
                        _enemyState.facingRight = false;
                    }
                }
                
                if (Vector3.Distance(transform.position, _positionSnapshot) < 8f && _disengage)
                {
                    _disengage = false;
                    state = State.Disengage;
                }
                
                else if (_hiveMind.CanAttack(5) && !(_attackTimer > 0))
                {
                    _attackTimer = 2f;
                    _animator.SetTrigger("Tornado");
                    _hiveMind.AddEncounterValue(5);
                    _hiveMind.ReturnToken(5);
                }
                stage = Stage.Exit;
            }
        }
        
        if (stage == Stage.Exit)
        {
            stage = Stage.Enter;
        }
    }

    private void Disengage()
    {
        if (stage == Stage.Enter)
        {
            _timer = 0.5f;
            stage = Stage.Update;
        }

        if (stage == Stage.Update)
        {
            if (_timer > 0)
            {
                if (!_enemyState.wallSensor)
                {
                    _agent.Move((transform.right * (Time.deltaTime * 10f))); 
                }
                _timer -= Time.deltaTime;
            }
            
            else 
            {
                stage = Stage.Exit;
            }
        }

        if (stage == Stage.Exit)
        {
            stage = Stage.Enter;
            state = State.Idle;
        }
    }

    public void FireProjectile(GameObject prefab, Transform transform, float speed)
    {
        var projectile = Instantiate(prefab, transform.position + new Vector3(0, 0.75f, 0), transform.rotation);
        projectile.GetComponent<Rigidbody>().velocity = 0.5f * speed * -transform.right;
    }
    
    public void TornadoAttack()
    {
        FireProjectile(tornado, transform, 15f);
    }
    
}
