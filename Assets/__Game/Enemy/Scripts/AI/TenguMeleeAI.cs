﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class TenguMeleeAI : MonoBehaviour
{
    private Animator _tengu;
    private GameObject _player;
    private NavMeshAgent _agent;
    private HiveMind _hiveMind;
    public EnemyState _enemyState;
    private Recoil _recoil;
    
    private enum Stage
    {
        Enter,
        Update,
        Exit
    }

    private enum State
    {
        Idle,
        Attack,
        Advance,
        Retreat,
        Wait,
        
    }
    [SerializeField] private Stage stage;
    [SerializeField] private State state;
    
    
    private bool _canProcess = true;

    private void Start()
    {
        stage = Stage.Enter;
        state = State.Idle;

        //_animator = GetComponent<Animator>();
        _tengu = GetComponentInChildren<Animator>();
        _player = GameObject.FindGameObjectWithTag("Player");
        _agent = GetComponent<NavMeshAgent>();
        _hiveMind = GetComponentInParent<HiveMind>();
        _enemyState = GetComponent<EnemyState>();
        _recoil = GetComponent<Recoil>();
        
   
    }
    
    private Vector3 Direction()
    {
        return _player.transform.position - transform.position;
    }

    private void FixedUpdate()
    {
        _recoil.ProcessRecoilTimer(_enemyState);

        if (_enemyState.dead)
        {
            gameObject.GetComponent<Collider>().isTrigger = true;
            _canProcess = false;
            _tengu.SetTrigger("Die");
            _enemyState.dead = false;
            Invoke(nameof(Die), 1f);
        }
    }

    private void Die()
    {
        gameObject.SetActive(false);
    }


    private void Update()
    {
        _recoil.ProcessRecoil(_enemyState, _agent);
        
        if (!_canProcess) return;
        
        if(Direction().x < 0)
            _recoil.SetDirectionModifier(Vector3.right);
        else _recoil.SetDirectionModifier(Vector3.left);

        if (_enemyState.stunned && !_tengu.GetCurrentAnimatorStateInfo(0).IsTag("Die"))
            _tengu.SetTrigger("Stun");
        
        else
        {
            if(state == State.Idle)
                Idle();
        
            if(state == State.Attack)
                Attack();
        
            if(state == State.Advance)
                Advance();
            
            if(state == State.Retreat)
                Retreat();
        }


    }

    private float _timer;
    private Vector3 _positionSnapshot;

    [SerializeField] private float debugDistance;

    private void Idle()
    {
        if (stage == Stage.Enter)
        {
            _positionSnapshot = _player.transform.position;

            _timer = 0.25f;
            
            _tengu.SetTrigger("Idle");
            stage = Stage.Update;
            
        }
        
        if (stage == Stage.Update)
        {
            if (_timer > 0)
            {
                _timer -= Time.deltaTime;
            }
            else
            {
                if (!_tengu.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
                {
                    if ((_player.transform.position - transform.position).x > 0)
                    {
                        transform.rotation = Quaternion.Euler(0, 180, 0);
                        _enemyState.facingRight = true;
                    }
                    
                    else if ((_player.transform.position - transform.position).x < 0)
                    {
                        transform.rotation = Quaternion.Euler(0, 0, 0);
                        _enemyState.facingRight = false;
                    }
                }

                else
                {
                    return;
                }
                
                if (Vector3.Distance(transform.position, _positionSnapshot) > 1 && !_enemyState.frontTouchingWall)
                {
                    var chance = Random.Range(0, 100);

                    if (_enemyState.blocked && !_enemyState.engaged && !_enemyState.backTouchingWall)
                        state = State.Retreat;

                    else if (chance > 70)
                        ;
                    else
                    {
                        state = State.Advance;
                    }
                }

                if (Vector3.Distance(transform.position, _positionSnapshot) < 1f || _enemyState.engaged)
                {
                    state = State.Attack;
                }
                   
                
                
                if (Math.Abs(Vector3.Distance(transform.position, _positionSnapshot) - (Vector3.Distance(transform.position, _player.transform.position))) < 0.1f)
                {
                    //Debug.Log("You have not moved.");
                }
                
                else if (Vector3.Distance(transform.position, _positionSnapshot) < (Vector3.Distance(transform.position, _player.transform.position)))
                {
                    //Debug.Log("No Running away");
                }

                else if (Vector3.Distance(transform.position, _positionSnapshot) > (Vector3.Distance(transform.position, _player.transform.position)))
                {
                    //Debug.Log("No coming closer");
                }

                stage = Stage.Exit;
            }
            
        }
        
        if (stage == Stage.Exit)
        {

            _tengu.ResetTrigger("Idle");
            stage = Stage.Enter;
        }
    }

    private void Attack()
    {
        if (stage == Stage.Enter)
        {
            var randomTimer = Random.Range(0, 3);
            _timer = randomTimer * 0.1f;
            stage = Stage.Update;
        }
        else if (stage == Stage.Update)
        {
            if (_timer > 0) 
                _timer -= Time.deltaTime;

            else
            {
                if (_hiveMind.CanAttack(1))
                {
                    //_animator.SetTrigger("Attack");
                    _tengu.SetTrigger(RandomAttack());
                    _hiveMind.AddEncounterValue(1);
                    _hiveMind.ReturnToken(1);
                }
                
                stage = Stage.Exit;
            }
            
        }

        else if (stage == Stage.Exit)
        {
            stage = Stage.Enter;
            state = State.Idle;
        }
    }
    
    

    private string RandomAttack()
    {
        switch (Random.Range(0, 4))
        {
            case 1:
                return "Attack";
            case 2:
                return "AttackLow";
            case 3:
                return "Combo1";
            case 4:
                return "Combo2";
            default:
                return null;
        }
    }

    [SerializeField] private float speed;
    private void Advance()
    {
        var randomSpeed = Random.Range(2, speed);
        
        if (stage == Stage.Enter)
        {
            _timer = 2f;
            stage = Stage.Update;
            _tengu.SetTrigger("Walk");
        }
        
        else if (stage == Stage.Update)
        {
            if (_timer > 0)
            {
                if ((_player.transform.position - transform.position).x > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                    _enemyState.facingRight = true;
                }
                    
                else if ((_player.transform.position - transform.position).x < 0)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    _enemyState.facingRight = false;
                }
                
                if (Vector3.Distance(transform.position, _player.transform.position) > 2)
                {
                    if (_enemyState.blocked || _enemyState.frontTouchingWall)
                        _timer = 0;
                    
                    if (!_enemyState.blocked && !_enemyState.frontTouchingWall)
                        _agent.Move((_player.transform.position - transform.position).normalized *
                                    (Time.deltaTime * randomSpeed));
                }
                
                _timer -= Time.deltaTime;
            }
            else
                stage = Stage.Exit;
        }
        
        else if (stage == Stage.Exit)
        {
            _tengu.SetTrigger("Idle");
            stage = Stage.Enter;
            state = State.Idle;
        }
        
    }
    
    
    private void Retreat()
    {
        var randomSpeed = Random.Range(2, speed);
        var randomTime = Random.Range(0.5f, 1.5f);

        if (stage == Stage.Enter)
        {
            _tengu.SetTrigger("Walk");
            _timer = randomTime;
            stage = Stage.Update;
        }
        
        else if (stage == Stage.Update)
        {
            if (_timer > 0)
            {
                if ( (_player.transform.position - transform.position).x > 0)
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                
                else if ((_player.transform.position - transform.position).x < 0)
                    transform.rotation = Quaternion.Euler(0, 0, 0);

                if (!_enemyState.backTouchingWall)
                {
                    _agent.Move(-(_player.transform.position - transform.position).normalized * (Time.deltaTime * randomSpeed * 0.5f));
                }
                
                _timer -= Time.deltaTime;
            }
            
            else
                stage = Stage.Exit;
        }
        
        else if (stage == Stage.Exit)
        {
            _tengu.ResetTrigger("Walk");
            stage = Stage.Enter;
            state = State.Idle;
        }
    }

}
