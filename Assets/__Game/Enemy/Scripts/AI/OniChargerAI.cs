﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class OniChargerAI : MonoBehaviour
{
    [SerializeField] private EnemyState enemyState;
    [SerializeField] private OniAnimations animations;
    [SerializeField] private OniWeaponry weaponry;

    [SerializeField] private NavMeshAgent agent;
    
    private float _moveTimer;
    private float _moveSpeed;
    private float _attackTimer;

    [SerializeField] private float walkSpeed;
    
    private HiveMind _hiveMind;
    private Recoil _recoil;

    private GameObject _player;
    private bool _canProcess = true;
    private float _timer;
    private Vector3 _positionSnapshot;
    
    private bool _canSpecial = true;



    private IEnumerator SpecialTimer()
    {
        _canSpecial = false;
        yield return new WaitForSeconds(5f);
        _canSpecial = true;
    }
    
    [SerializeField] private float attackCooldown;
    [SerializeField] private bool playerComingTowards;
    [SerializeField] private bool playerRunningAway;
    [SerializeField] private bool playerStanding;

    [SerializeField] private Stage stage = Stage.Enter;
    private enum Stage
    {
        Enter,
        Update,
        Exit
    }

    [SerializeField] private State state = State.Idle;
    private enum State
    {
        Idle, Walk, Attack, Charge
    }

    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _recoil = GetComponent<Recoil>();
        _hiveMind = GetComponentInParent<HiveMind>();
    }

    private void FixedUpdate()
    {
        PlayerBooleans();
        Move();

        if (enemyState.stunned)
        {
            stage = Stage.Enter;
            state = State.Idle;
        }
        
        _recoil.ProcessRecoilTimer(enemyState);
        _recoil.ProcessRecoil(enemyState, agent);
        
        if (enemyState.dead)
        {
            _canProcess = false;
            enemyState.dead = false;
            animations.Die();
            Invoke(nameof(Die), 1f);
        }
    }

    private void Update()
    {
        AttackTimer();
        SpecialTimer();
        FrontalRay();
        
        if (!_canProcess) return;

        else
        {
            if(state == State.Idle)
                Idle();
        
            if(state == State.Attack)
                Attack();
        
            if(state == State.Walk)
                Walk();
            
            if(state == State.Charge)
                Charge();
        }
        
    }

    private void Idle()
    {
        if (stage == Stage.Enter)
        { 
            // animations.Idle();
            animations.ResetCharge();
            animations.ResetChargeEnd();
            
            _positionSnapshot = _player.transform.position;
            _timer = 0.25f;

            
            
            stage = Stage.Update;
        }

        if (stage == Stage.Update)
        {
            if (_timer > 0) { _timer -= Time.deltaTime; }
            
            else
            {
                RotateTowardsPlayer();

                
                if (Vector3.Distance(transform.position, _positionSnapshot) < 5f && enemyState.canAttack)
                {
                    enemyState.canAttack = false;
                    _attackTimer = attackCooldown;
                    state = State.Attack;
                }
                
                else if (Vector3.Distance(transform.position, _positionSnapshot) < 10f && enemyState.canTurn)
                {
                    state = State.Walk;
                }
                
                else if ((Vector3.Distance(transform.position, _positionSnapshot)  >10f && enemyState.canTurn && _canSpecial))
                {
                    StartCoroutine(SpecialTimer());
                    state = State.Charge;
                }
                
                
                
                stage = Stage.Exit;
            }
        }

        if (stage == Stage.Exit)
        {
            animations.ResetIdle();
            
            
            
            stage = Stage.Enter;
        }
    }

    private void Attack()
    {
        if (stage == Stage.Enter)
        {
            _timer = 0.1f;
            
            
            
            stage = Stage.Update;
        }

        if (stage == Stage.Update)
        {
            if(_timer > 0)
            {
                _timer -= Time.deltaTime;
            }

            else
            {
                
                animations.Attack();

                _moveSpeed = 2.5f;
                _moveTimer = 0.4f;
                
                
                
                stage = Stage.Exit;
            }
        }

        if (stage == Stage.Exit)
        {
            
            animations.Idle();
            state = State.Idle;
            stage = Stage.Enter;
        }
    }

    private void Walk()
    {
        if (stage == Stage.Enter)
        {
            animations.Walk();
            animations.WalkMultiplier(1f);
            _timer = 3f;

            
            
            stage = Stage.Update;
        }

        if (stage == Stage.Update)
        {
            if (_timer > 0)
            {
                _timer -= Time.deltaTime;
                RotateTowardsPlayer();


               
                
                agent.Move( (_player.transform.position-transform.position).normalized * (Time.deltaTime * walkSpeed));
             
 

                if (Vector3.Distance(transform.position, _player.transform.position) < 3)
                {
                    
                    
                    
                    stage = Stage.Exit;
                }
            }
            else
            {
                
                
                
                stage = Stage.Exit;
            }
       
        }

        if (stage == Stage.Exit)
        {
            animations.ResetWalk();
            animations.Idle();
            
            
            state = State.Idle;
            stage = Stage.Enter;
        }
    }

    private void Move()
    {
        if (_moveTimer > 0)
        {
            _moveTimer -= Time.deltaTime;

            if (!enemyState.stunned)
            {
                agent.Move(-transform.right * (_moveSpeed * Time.deltaTime));
            }
        }
    }


    private void AttackTimer()
    {
        if (_attackTimer > 0)
        {
            _attackTimer -= Time.deltaTime;
        }
        else
        {
            enemyState.canAttack = true;
        }
    }
    

    private void Die()
    {
        gameObject.SetActive(false);
    }
    
    
    private Vector3 Direction() { return _player.transform.position - transform.position; }

    private void RotateTowardsPlayer()
    {
        if (enemyState.canTurn && !animations.ChargeRoar() && !animations.ChargeStun())
        {
            if ((_player.transform.position - transform.position).x > 0)
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
                enemyState.facingRight = true;
                
                
                weaponry.WeaponL();
            }
                    
            else if ((_player.transform.position - transform.position).x < 0)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
                enemyState.facingRight = false;

                weaponry.WeaponR();
            }
        }
        else
        {
            return;
        }
    }


    [SerializeField] private float chargeSpeed;
    private void Charge()
    {
         if (stage == Stage.Enter)
         {
             enemyState.charge = true;
             
             RotateTowardsPlayer(); 
             animations.Charge();

             stage = Stage.Update;
         }

         if (stage == Stage.Update)
         {
             if(!animations.ChargeRoar())
             {
                 enemyState.charge = true;
                 
                 agent.Move( (-transform.right * (Time.deltaTime * chargeSpeed)));
             
                 if (enemyState.frontTouchingWall)
                 {
                     animations.ChargeEnd();
                     
                     
                     
                     stage = Stage.Exit;
                 }
             }
             
         }

         if (stage == Stage.Exit)
         {
             if (animations.ChargeStun())
             {
                 ;
             }
             else
             {
                 enemyState.charge = false;
                 state = State.Idle;
                 stage = Stage.Enter;
             }
         }
    }

    private RaycastHit _frontCast;

    private void FrontalRay()
    {
        if (Physics.Raycast(transform.position, -transform.right, out _frontCast, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, (-transform.right )*_frontCast.distance, Color.red);
            enemyState.frontTouchingWall =
                (_frontCast.collider.gameObject.layer == LayerMask.NameToLayer("Wall") || _frontCast.collider.gameObject.layer == LayerMask.NameToLayer("EnemyWall") ) && _frontCast.distance < 1.5;
        }
    }
    

    private void PlayerBooleans()
    {
        var position = transform.position;
        var playerPosition = _player.transform.position;
        playerStanding = Math.Abs(Vector3.Distance(position, _positionSnapshot) -
                                   (Vector3.Distance(position, playerPosition))) < 0.1f;
        playerComingTowards = Vector3.Distance(position, _positionSnapshot) >
                               (Vector3.Distance(position, playerPosition));
        playerRunningAway = Vector3.Distance(position, _positionSnapshot) <
                             (Vector3.Distance(position, playerPosition));
    }
}
