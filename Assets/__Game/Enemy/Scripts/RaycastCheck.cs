﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastCheck : MonoBehaviour
{
    public EnemyState state;
    
    private bool _pathBlocked;
    private bool _closeToPlayer;

    private Transform _player;

    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player").transform;
    }


    private void Update()
    {
        state.blocked = _pathBlocked;
        state.engaged = _closeToPlayer;
    }

    private void FixedUpdate()
    {

        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.right, out hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, (-transform.right )*hit.distance, Color.white);

            if (hit.collider.gameObject.CompareTag("Player") && hit.distance < 2)
            {
                _closeToPlayer = true;
            }
            else _closeToPlayer = false;

            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Enemy") && hit.distance < 1 && !state.engaged)
            {
                _pathBlocked = true;
            }
            else _pathBlocked = false;
            
            
            state.frontTouchingWall =
                hit.collider.gameObject.layer == LayerMask.NameToLayer("Wall") && hit.distance < 1;
        }

        RaycastHit hit2;
        if (Physics.Raycast(transform.position, transform.right, out hit2, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, (transform.right )*hit2.distance, Color.white);
            state.frontTouchingWall =
                hit2.collider.gameObject.layer == LayerMask.NameToLayer("Wall") && hit.distance < 1;
        }
        
        
    }
}
