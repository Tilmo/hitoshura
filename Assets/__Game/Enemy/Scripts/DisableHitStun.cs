﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableHitStun : MonoBehaviour
{
    public void HitStunDisable()
    {
        this.gameObject.SetActive(false);
    }
}
