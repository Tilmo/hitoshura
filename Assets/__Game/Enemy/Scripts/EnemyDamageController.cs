﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace __Game.Enemy.Scripts
{
    public class EnemyDamageController : MonoBehaviour
    {
        public int currentHealthIndex;
        public List<int> health = new List<int>();
        public Material material1;
        public Material material2;
        public Material material3;
        public Material material4;

        private MeshRenderer _meshRenderer;
        private bool _cantProcessColour;
        
        public EnemyState state;

        public float stunDuration;

        public delegate  void Die();

        public delegate void Combo();
        public static event Die EnemyDie;
        public static event Combo HitCombo;

        [SerializeField] private Animator animator;
    

        private void Start()
        {
            currentHealthIndex = 0;
            _meshRenderer = GetComponent<MeshRenderer>();
            state = GetComponentInParent<EnemyState>();

            CheckColor();
        }

        private void Update()
        {
            if (stunDuration > 0)
            {
                stunDuration -= Time.deltaTime;
            }
            else
            {
                state.stunned = false;
            }
        }


        public void TakeDamage(int attack)
        {
            if (health[currentHealthIndex] == attack)
            {
                currentHealthIndex++;
                state.stunned = true;
                state.recoilX = true;
                stunDuration = 0.25f;
                HitCombo?.Invoke();

                animator.SetTrigger("Hit");
            }

            if (currentHealthIndex >= health.Count)
            {
                currentHealthIndex = 0;
                state.stunned = false;
                
                state.dead = true;

                _cantProcessColour = true;
                EnemyDie?.Invoke();
            }
            CheckColor();
        }



        private void CheckColor()
        {
            if (_cantProcessColour)
            {
                _meshRenderer.material = material4;
            }
            else
            {
                if (health[currentHealthIndex] == 0)
                {
                    _meshRenderer.material = material1;
                }
                if (health[currentHealthIndex] == 1)
                {
                    _meshRenderer.material = material2;
                }
                if (health[currentHealthIndex] == 2)
                {
                    _meshRenderer.material = material3;
                }  
            }
            
        }
    
    }
}
