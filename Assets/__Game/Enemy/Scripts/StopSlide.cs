﻿using UnityEngine;

namespace __Game.Enemy.Scripts
{
    public class StopSlide : MonoBehaviour
    {
        private void OnCollisionExit(Collision other)
        {
            other.rigidbody.velocity = new Vector3(0, other.relativeVelocity.y, 0);
        }
        
    }
}
