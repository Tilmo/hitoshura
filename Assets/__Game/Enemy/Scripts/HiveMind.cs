﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiveMind : MonoBehaviour
{
    public int encounterBudget;
    public int currentEncounterValue;
    
    public void AddEncounterValue(int value)
    {
        currentEncounterValue += value;
    }

    public bool CanAttack(int value)
    {
        return !( (currentEncounterValue + value) > encounterBudget);
    }

    public void ReturnToken(int token)
    {
        StartCoroutine(ReturnAttackToken(token));
    }
    
    private IEnumerator ReturnAttackToken(int token)
    {
        yield return new WaitForSeconds(2f);
        AddEncounterValue(-token);
    }
    
}
