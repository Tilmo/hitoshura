﻿using System;
using System.Collections;
using System.Collections.Generic;
using __Game.Enemy.Scripts;
using UnityEngine;
using Random = UnityEngine.Random;

public class WaveManager : MonoBehaviour
{

    [SerializeField] private int enemiesAlive;
    [SerializeField] private List<GameObject> enemies;

    private EncounterController _encounterController;

    private void Start()
    {
        enemiesAlive = enemies.Count;
        _encounterController = GetComponentInParent<EncounterController>();
    }

    private void OnEnable()
    {
        EnemyDamageController.EnemyDie += KillEnemy;
        StartCoroutine(ActivateRandom(enemies));
    }
    
    private void OnDisable()
    {
        EnemyDamageController.EnemyDie -= KillEnemy;
    }

    
    IEnumerator ActivateRandom(List<GameObject> enemies)
    {
        foreach (var enemy in enemies)
        {
            var randomWait = Random.Range(0.1f, 0.5f);
            enemy.SetActive(true);
            yield return new WaitForSeconds(randomWait);
        }
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }
    
    private void KillEnemy()
    {
        enemiesAlive--;

        if (enemiesAlive <= 0)
        {
            _encounterController.ProgressWaves();
           Invoke(nameof(Disable), 1f);
        }
            
    }
}
