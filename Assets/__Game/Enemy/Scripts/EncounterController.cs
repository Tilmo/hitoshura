﻿using System;
using System.Collections;
using System.Collections.Generic;
using __Game.Enemy.Scripts;
using __Game.Player.Scripts;
using UnityEngine;
using Random = UnityEngine.Random;

public class EncounterController : MonoBehaviour
{

    [SerializeField] private GameObject walls;
    private PlayerState _playerState;

    [SerializeField] private int waveIndex;
    [SerializeField] private List<GameObject> waves;

    private void Awake()
    {
        _playerState = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerState>();
    }

    private void Walls(bool closed)
    {
        walls.SetActive(closed);
    }

    public void ProgressWaves()
    {
        if(waveIndex >= waves.Count)
            EndEncounter();

        else
        {
            waves[waveIndex].SetActive(true);
            waveIndex++;
        }
    }

    public void EndEncounter()
    {
        Walls(false);
        _playerState.encounter = false;

        Time.timeScale = 0.2f;
        _timeScaleTrigger = true;
        _timeScaleTimer = .3f;
    }
    public void StartEncounter()
    {
        Walls(true);
        _playerState.encounter = true;
        ProgressWaves();
    }

    private float _timeScaleTimer;
    private bool _timeScaleTrigger;

    private void Update()
    {
        if (_timeScaleTrigger)
        {
            if (_timeScaleTimer > 0)
            {
                _timeScaleTimer -= Time.deltaTime;
            }
            else
            {
                _timeScaleTrigger = false;
                Time.timeScale = 1f;
            }
        }
       
    }
}
