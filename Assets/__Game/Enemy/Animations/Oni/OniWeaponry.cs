﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OniWeaponry : MonoBehaviour
{
    [SerializeField] private GameObject weaponL;
    [SerializeField] private GameObject weaponR;
    
    [SerializeField] private GameObject clubL;
    [SerializeField] private GameObject clubR;
    [SerializeField] private GameObject spikeClubL;
    [SerializeField] private GameObject spikeClubR;
    [SerializeField] private GameObject rockL;
    [SerializeField] private GameObject rockR;

    
    
    [SerializeField] private bool left;
    [SerializeField] private bool right;
    
    
    private void SpikeClubsActive(bool active)
    {
        spikeClubL.SetActive(active);
    }

    private void ClubsActive(bool active)
    {
        clubL.SetActive(active);
    }

    private void RocksActive(bool active)
    {
        rockL.SetActive(active);
    }

    public void RockSwitch()
    {
        rockL.SetActive(!rockL.activeSelf);
        rockR.SetActive(!rockR.activeSelf);
    }

    public void ClubSwitch()
    {
        clubL.SetActive(!clubL.activeSelf);
        clubR.SetActive(!clubR.activeSelf);
    }

    public void SpikeClubSwitch()
    {
        spikeClubL.SetActive(!spikeClubL.activeSelf);
        spikeClubR.SetActive(!spikeClubR.activeSelf);
    }

    public void ClubL()
    {
        if (left) return;
        left = true;
        right = false;
        clubL.SetActive(true);
        clubR.SetActive(false);
    }

    public void ClubR()
    {
        if (right) return; 
        left = false;
        right = true;
        clubL.SetActive(false);
        clubR.SetActive(true);
    }
    
    public void WeaponL()
    {
        if (left) return;
        left = true;
        right = false;
        weaponL.SetActive(true);
        weaponR.SetActive(false);
    }

    public void WeaponR()
    {
        if (right) return; 
        left = false;
        right = true;
        weaponL.SetActive(false);
        weaponR.SetActive(true);
    }
    
    public void SpikeClubR()
    {
        if (left) return;
        left = true;
        right = false;
        spikeClubL.SetActive(true);
        spikeClubR.SetActive(false);
    }

    public void SpikeCLubL()
    {
        if (right) return; 
        left = false;
        right = true;
        spikeClubL.SetActive(false);
        spikeClubR.SetActive(true);
    }
    
}
