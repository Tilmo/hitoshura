﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public class OniAnimations : MonoBehaviour
{
    public Animator animator;
    [SerializeField] private EnemyState enemyState;

    private float _layerWeight;

    private void FixedUpdate()
    {
        gameObject.transform.rotation = Quaternion.LookRotation(Vector3.forward);
        animator.SetLayerWeight(1, enemyState.facingRight ? 1 : 0);
        animator.SetLayerWeight(2, !enemyState.facingRight ? 1 : 0);
        enemyState.canTurn = CanTurn();
        enemyState.attackAnimation = CanTurn();
        Stunned(enemyState.stunned);
        
    }

    public void Attack() { animator.SetTrigger("Attack"); }
    public void Stun() { animator.SetTrigger("Stun"); }
    public void Die() { animator.SetTrigger("Die"); }
    public void Walk() { animator.SetTrigger("Walk"); }
    public void Idle() { animator.SetTrigger("Idle"); }
    
    public void ResetIdle() { animator.ResetTrigger("Idle");}
    public void ResetWalk() { animator.ResetTrigger("Walk");}

    public void WalkMultiplier(float walk) { animator.SetFloat("WalkMultiplier", walk); }

    public void Stunned(bool stunned) { animator.SetBool("Stunned", stunned);}
    public bool CanTurn() { return !animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"); }
    
    public bool ChargeRoar() { return animator.GetCurrentAnimatorStateInfo(0).IsTag("ChargeStart");}
    public bool ChargeStun() { return animator.GetCurrentAnimatorStateInfo(0).IsTag("ChargeEnd" );}
    
    public void Charge() { animator.SetTrigger("Charge");}
    public void ChargeEnd(){animator.SetTrigger("ChargeEnd");}
    public void ResetCharge() { animator.ResetTrigger("Charge");}
    public void ResetChargeEnd(){animator.ResetTrigger("ChargeEnd");}


    [SerializeField] private float chargeIntensity;
    [SerializeField] private float collisionIntensity;
    [SerializeField] private float chargeTime;
    [SerializeField] private float collisionTime;
    public void ShakeCamera()
    {
        CinemachineShake.instance.ShakeCamera(chargeIntensity,chargeTime);
    }
    public void ShakeCameraCollision()
    {
        CinemachineShake.instance.ShakeCamera(collisionIntensity,collisionTime);
    }

    

}
