﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TenguRangedAeromancerAnimations : MonoBehaviour
{
    private Animator _animator;
    private TenguRangedAeromancerAI _lesserTenguRangedAeromancerAI;
    
    [SerializeField] private GameObject fanLeft;
    [SerializeField] private GameObject fanRight;
    
    private void Start()
    {
        _animator = GetComponent<Animator>();
        _lesserTenguRangedAeromancerAI = GetComponentInParent<TenguRangedAeromancerAI>();
        
    }

    private void FixedUpdate()
    {
        gameObject.transform.rotation = Quaternion.LookRotation(Vector3.forward);
        if (_lesserTenguRangedAeromancerAI._enemyState.facingRight)
        {
            _animator.SetLayerWeight(1, 1);
        }
        else
        {
            _animator.SetLayerWeight(1, 0);
        }
    }

    public void TornadoAttack()
    {
        _lesserTenguRangedAeromancerAI.TornadoAttack();
    }
    
    
    
}


