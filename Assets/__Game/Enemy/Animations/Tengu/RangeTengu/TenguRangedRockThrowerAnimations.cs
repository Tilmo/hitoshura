﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TenguRangedRockThrowerAnimations : MonoBehaviour
{

    private Animator _animator;
    private TenguRockThrowerAI _rockThrowerAI;

    
    private void Start()
    {
        _animator = GetComponent<Animator>();
        _rockThrowerAI = GetComponentInParent<TenguRockThrowerAI>();
    }

    private void FixedUpdate()
    {
        gameObject.transform.rotation = Quaternion.LookRotation(Vector3.forward);
        if (_rockThrowerAI.enemyState.facingRight)
        {
            _animator.SetLayerWeight(1, 1);
        }
        else
        {
            _animator.SetLayerWeight(1, 0);
        }
    }

    public void ThrowRock()
    {
        _rockThrowerAI.ThrowRock();
    }
}
