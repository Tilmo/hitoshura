﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TenguMeleeAnimations : MonoBehaviour
{
    private Animator _animator;
    private TenguMeleeAI _tenguMeleeAI;

    public bool attacking;

    private bool _nullAI;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _tenguMeleeAI = GetComponentInParent<TenguMeleeAI>();

        if (_tenguMeleeAI == null)
        {
            _nullAI = true;
        }
    }

    private void FixedUpdate()
    {
        if(_nullAI) return;
        
        Attacking();
        gameObject.transform.rotation = Quaternion.LookRotation(Vector3.forward);
        _animator.SetLayerWeight(1, _tenguMeleeAI._enemyState.facingRight ? 1 : 0);
    }

    public void Attacking()
    {
        _tenguMeleeAI._enemyState.attacking = attacking;
    }
}
