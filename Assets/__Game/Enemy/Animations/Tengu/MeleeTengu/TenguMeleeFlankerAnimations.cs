﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TenguMeleeFlankerAnimations : MonoBehaviour
{
    private Animator _animator;
    private TenguMeleeFlankerAI _meleeFlankerAI;
    private TenguMeleeAnimations _tenguMeleeAnimations;
    private void Start()
    {
        _animator = GetComponent<Animator>();
        _meleeFlankerAI = GetComponentInParent<TenguMeleeFlankerAI>();
        _tenguMeleeAnimations = GetComponent<TenguMeleeAnimations>();
    }

    private void FixedUpdate()
    {
        Attacking();
        
        gameObject.transform.rotation = Quaternion.LookRotation(Vector3.forward);
        if (_meleeFlankerAI._enemyState.facingRight)
        {
            _animator.SetLayerWeight(1, 1);
        }
        else
        {
            _animator.SetLayerWeight(1, 0);
        }
    }

    public void Attacking()
    {
        _meleeFlankerAI._enemyState.attacking = _tenguMeleeAnimations.attacking;
    }
}
