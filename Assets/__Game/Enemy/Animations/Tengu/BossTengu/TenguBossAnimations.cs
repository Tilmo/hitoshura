﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TenguBossAnimations : MonoBehaviour
{
    private Animator _animator;
    private TenguBossAI _bossAI;

    [SerializeField] private Transform defaultTransform;
    [SerializeField] private float flySpeed;
    private float x;



    private void Start()
    {
        _animator = GetComponent<Animator>();
        _bossAI = GetComponentInParent<TenguBossAI>();

        x = transform.position.x;

    }

    private void FixedUpdate()
    {
        gameObject.transform.rotation = Quaternion.LookRotation(Vector3.forward);
        if (_bossAI.enemyState.facingRight)
        {
            _animator.SetLayerWeight(1, 1);
        }
        else
        {
            _animator.SetLayerWeight(1, 0);
        }

        if (_animator.GetCurrentAnimatorStateInfo(0).IsTag("Fly"))
        {
            transform.Translate(Vector3.up * (Time.deltaTime * flySpeed) );
        }
        else
        {
            transform.position = defaultTransform.position;
        }

    }
    
}
