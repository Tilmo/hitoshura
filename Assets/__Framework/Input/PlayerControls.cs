// GENERATED AUTOMATICALLY FROM 'Assets/PlayerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControls"",
    ""maps"": [
        {
            ""name"": ""gameplay"",
            ""id"": ""8258406d-a3b3-41b4-8272-4f90138f1aec"",
            ""actions"": [
                {
                    ""name"": ""jump"",
                    ""type"": ""Button"",
                    ""id"": ""8e5435ec-d129-4cb5-a856-d099c0949fcb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""move"",
                    ""type"": ""Button"",
                    ""id"": ""2a3fa94d-0164-4898-8c3d-e8ca9baab83c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""roll"",
                    ""type"": ""Button"",
                    ""id"": ""2bef061b-b78b-478c-a2f4-b4989b9e17b7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""fastfall"",
                    ""type"": ""Button"",
                    ""id"": ""b4ccce11-3228-4d83-84d0-f124cfb0edb1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""slowfall"",
                    ""type"": ""Button"",
                    ""id"": ""42126077-6b7d-4bd4-aa4c-79031286b05f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""attack1"",
                    ""type"": ""Button"",
                    ""id"": ""14265e1c-48ad-4887-878a-d427dc70ff7f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""attack2"",
                    ""type"": ""Button"",
                    ""id"": ""785ed1c2-e032-460f-ba3e-1b7ec2669025"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""attack3"",
                    ""type"": ""Button"",
                    ""id"": ""88803f15-fdc2-4cf8-9f68-cbcf4dd2de59"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""87e808fb-6ef6-441e-b866-2c117dcc01ae"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6b504627-af50-4017-9089-49f5f670c5e1"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ad0e57ef-31b2-43c9-ac09-dc5449daf9b0"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""roll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""49884d1e-a8e3-41a0-89d3-87f931ab8738"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""roll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a0fd9c1f-78d8-4e3c-a0a6-ce774766c283"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""roll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Left Stick"",
                    ""id"": ""bf536aa1-cfe3-4762-9a4c-9cc0dd58ed52"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": ""AxisDeadzone(min=0.4)"",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""5d1b8408-90d9-4b47-8211-4da191473939"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""307bf729-f72b-49c4-bede-9bf5af742c14"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Arrow Keys"",
                    ""id"": ""d04130c5-f991-476d-a57c-a5ca3b682042"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""b9bbc956-3d6c-4d6c-a480-2ccf43f1f230"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""0f8053ed-6163-47db-8fb6-4eb25a7f44c8"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""950bc785-a086-4693-b7d0-555119a75609"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""fastfall"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7519f750-21bf-4d0b-b8fe-01f40390abe1"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""fastfall"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7001191c-8039-40dc-a459-da3ab4ed0508"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""slowfall"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""979bc166-7838-4e9c-9395-07d361629292"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""slowfall"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""766dc09f-1a00-4649-98ed-f36abc9b0bd5"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""attack1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9360bd3a-03ad-47ea-bf56-1443799a9b3b"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""attack1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dfbdd93a-f7f6-485c-bb07-9f8e99cc9056"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""attack2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4acd8137-d314-4b96-8526-96468fc438fc"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""attack2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c08c8ce0-5480-4084-bebb-c9bfb1c915b9"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""attack3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dc84aafd-e405-4e02-8091-84926421d0e5"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""attack3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // gameplay
        _mGameplay = asset.FindActionMap("gameplay", throwIfNotFound: true);
        _mGameplayJump = _mGameplay.FindAction("jump", throwIfNotFound: true);
        _mGameplayMove = _mGameplay.FindAction("move", throwIfNotFound: true);
        _mGameplayRoll = _mGameplay.FindAction("roll", throwIfNotFound: true);
        _mGameplayFastfall = _mGameplay.FindAction("fastfall", throwIfNotFound: true);
        _mGameplaySlowfall = _mGameplay.FindAction("slowfall", throwIfNotFound: true);
        _mGameplayAttack1 = _mGameplay.FindAction("attack1", throwIfNotFound: true);
        _mGameplayAttack2 = _mGameplay.FindAction("attack2", throwIfNotFound: true);
        _mGameplayAttack3 = _mGameplay.FindAction("attack3", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // gameplay
    private readonly InputActionMap _mGameplay;
    private IGameplayActions _mGameplayActionsCallbackInterface;
    private readonly InputAction _mGameplayJump;
    private readonly InputAction _mGameplayMove;
    private readonly InputAction _mGameplayRoll;
    private readonly InputAction _mGameplayFastfall;
    private readonly InputAction _mGameplaySlowfall;
    private readonly InputAction _mGameplayAttack1;
    private readonly InputAction _mGameplayAttack2;
    private readonly InputAction _mGameplayAttack3;
    public struct GameplayActions
    {
        private @PlayerControls _mWrapper;
        public GameplayActions(@PlayerControls wrapper) { _mWrapper = wrapper; }
        public InputAction @jump => _mWrapper._mGameplayJump;
        public InputAction @move => _mWrapper._mGameplayMove;
        public InputAction @roll => _mWrapper._mGameplayRoll;
        public InputAction @fastfall => _mWrapper._mGameplayFastfall;
        public InputAction @slowfall => _mWrapper._mGameplaySlowfall;
        public InputAction @attack1 => _mWrapper._mGameplayAttack1;
        public InputAction @attack2 => _mWrapper._mGameplayAttack2;
        public InputAction @attack3 => _mWrapper._mGameplayAttack3;
        public InputActionMap Get() { return _mWrapper._mGameplay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
        public void SetCallbacks(IGameplayActions instance)
        {
            if (_mWrapper._mGameplayActionsCallbackInterface != null)
            {
                @jump.started -= _mWrapper._mGameplayActionsCallbackInterface.OnJump;
                @jump.performed -= _mWrapper._mGameplayActionsCallbackInterface.OnJump;
                @jump.canceled -= _mWrapper._mGameplayActionsCallbackInterface.OnJump;
                @move.started -= _mWrapper._mGameplayActionsCallbackInterface.OnMove;
                @move.performed -= _mWrapper._mGameplayActionsCallbackInterface.OnMove;
                @move.canceled -= _mWrapper._mGameplayActionsCallbackInterface.OnMove;
                @roll.started -= _mWrapper._mGameplayActionsCallbackInterface.OnRoll;
                @roll.performed -= _mWrapper._mGameplayActionsCallbackInterface.OnRoll;
                @roll.canceled -= _mWrapper._mGameplayActionsCallbackInterface.OnRoll;
                @fastfall.started -= _mWrapper._mGameplayActionsCallbackInterface.OnFastfall;
                @fastfall.performed -= _mWrapper._mGameplayActionsCallbackInterface.OnFastfall;
                @fastfall.canceled -= _mWrapper._mGameplayActionsCallbackInterface.OnFastfall;
                @slowfall.started -= _mWrapper._mGameplayActionsCallbackInterface.OnSlowfall;
                @slowfall.performed -= _mWrapper._mGameplayActionsCallbackInterface.OnSlowfall;
                @slowfall.canceled -= _mWrapper._mGameplayActionsCallbackInterface.OnSlowfall;
                @attack1.started -= _mWrapper._mGameplayActionsCallbackInterface.OnAttack1;
                @attack1.performed -= _mWrapper._mGameplayActionsCallbackInterface.OnAttack1;
                @attack1.canceled -= _mWrapper._mGameplayActionsCallbackInterface.OnAttack1;
                @attack2.started -= _mWrapper._mGameplayActionsCallbackInterface.OnAttack2;
                @attack2.performed -= _mWrapper._mGameplayActionsCallbackInterface.OnAttack2;
                @attack2.canceled -= _mWrapper._mGameplayActionsCallbackInterface.OnAttack2;
                @attack3.started -= _mWrapper._mGameplayActionsCallbackInterface.OnAttack3;
                @attack3.performed -= _mWrapper._mGameplayActionsCallbackInterface.OnAttack3;
                @attack3.canceled -= _mWrapper._mGameplayActionsCallbackInterface.OnAttack3;
            }
            _mWrapper._mGameplayActionsCallbackInterface = instance;
            if (instance != null)
            {
                @jump.started += instance.OnJump;
                @jump.performed += instance.OnJump;
                @jump.canceled += instance.OnJump;
                @move.started += instance.OnMove;
                @move.performed += instance.OnMove;
                @move.canceled += instance.OnMove;
                @roll.started += instance.OnRoll;
                @roll.performed += instance.OnRoll;
                @roll.canceled += instance.OnRoll;
                @fastfall.started += instance.OnFastfall;
                @fastfall.performed += instance.OnFastfall;
                @fastfall.canceled += instance.OnFastfall;
                @slowfall.started += instance.OnSlowfall;
                @slowfall.performed += instance.OnSlowfall;
                @slowfall.canceled += instance.OnSlowfall;
                @attack1.started += instance.OnAttack1;
                @attack1.performed += instance.OnAttack1;
                @attack1.canceled += instance.OnAttack1;
                @attack2.started += instance.OnAttack2;
                @attack2.performed += instance.OnAttack2;
                @attack2.canceled += instance.OnAttack2;
                @attack3.started += instance.OnAttack3;
                @attack3.performed += instance.OnAttack3;
                @attack3.canceled += instance.OnAttack3;
            }
        }
    }
    public GameplayActions @gameplay => new GameplayActions(this);
    public interface IGameplayActions
    {
        void OnJump(InputAction.CallbackContext context);
        void OnMove(InputAction.CallbackContext context);
        void OnRoll(InputAction.CallbackContext context);
        void OnFastfall(InputAction.CallbackContext context);
        void OnSlowfall(InputAction.CallbackContext context);
        void OnAttack1(InputAction.CallbackContext context);
        void OnAttack2(InputAction.CallbackContext context);
        void OnAttack3(InputAction.CallbackContext context);
    }
}
