﻿using System;
using System.Collections;
using System.Collections.Generic;
using __Game.Player.Scripts;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    public PlayerController playerController;
    public GameObject player;

    public float smoothSpeed = 0.125f;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerController = player.GetComponent<PlayerController>();
    }

    private void Update()
    {
        smoothSpeed = SmoothingSpeed();
        
        var desidedPosition = target.position + offset;
        var smoothedPosition = Vector3.Lerp(transform.position, desidedPosition, smoothSpeed);
        transform.position = smoothedPosition;
    }

    private float SmoothingSpeed()
    {
        return 0.125f;
    }

    
}
